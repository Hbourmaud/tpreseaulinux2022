# TP4 : TCP, UDP et services réseau
## I. First steps
Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP  

*Discord (quand on envoie des messages), c'est de l'UDP (en vrai c'est plutôt du QUIC)*  

*Ip auquel on peut se connecter : 162.159.128.233 sur le port 443*  
*Le port local qui a été ouvert ici est le 65032*  

Avis de l'OS
```bash=
PS C:\WINDOWS\system32> netstat -a -b -p UDP

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
  UDP    0.0.0.0:65032          *:*
 [Discord.exe]
```

🦈[PCAP Discord](./PCAP/tp4_discordmsg.pcapng)

*Microsoft Store c'est du TCP*  

*Ip auquel on peut se connecter : 20.99.128.106 sur le port 443*  
*Le port local qui a été ouvert ici est le 1404*  

Avis de l'OS
```bash=
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
 TCP    10.33.16.31:1404       20.99.128.106:443      TIME_WAIT
 TCP    10.33.16.31:1405       23.57.5.23:443         ESTABLISHED
 [WinStore.App.exe]
...
```
🦈[PCAP Microsoft Store](./PCAP/tp4_microsoftstore.pcapng)

*Minecraft en Multi c'est du TCP*  
*Ip auquel on peut se connecter : 185.116.156.246 sur le port 37629*  
*Le port local qui a été ouvert ici est le 51902*  

Avis de l'OS
```bash=
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
 TCP    192.168.137.145:51902  185.116.156.246:37629  ESTABLISHED
 [javaw.exe]
...
```
🦈[PCAP Minecraft](./PCAP/tp4_mc.pcapng)

*Steam quand ça télécharge un jeu c'est de l'UDP (entre autres, parce qu'il y a beaucoup de connection qui s'effectue comme du web ...)*  
*Ip auquel on peut se connecter : 185.25.182.76 sur le port 27017*  
*Le port local qui a été ouvert ici est le 55551*  

Avis de l'OS
```bash=
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
 UDP    0.0.0.0:55551          :
 [steam.exe]
...
```
🦈[PCAP Steam](./PCAP/tp4_steamdl.pcapng)

*Une page web comme Hyperplanning c'est du TCP*  
*Ip auquel on peut se connecter : 96.16.122.67 sur le port 80*  
*Plusieurs ports ont été ouvert ici tels que 50820/1/2/3*  

Avis de l'OS
```bash=
PS C:\WINDOWS\system32> netstat -a -b -n

Active Connections

  Proto  Local Address          Foreign Address        State
  ...
  TCP    192.168.137.145:50820  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50821  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50822  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50823  96.16.122.67:80        TIME_WAIT
  TCP    192.168.137.145:50826  178.32.154.10:443      ESTABLISHED
 [brave.exe]
...
```
*(J'utilise Brave en navigateur.)*

🦈[PCAP Hyperplanning](./PCAP/tp4_web.pcapng)

## II. Mise en place
### 1. SSH
Examinez le trafic dans Wireshark  

*évidemment SSH utilise TCP parce qu'on veut que les commandes qu'on envoit soit intègre et qu'elle n'arrive pas dans le désordre par exemple.*  

🦈[PCAP SSH](./PCAP/tp4_ssh.pcapng)

Demandez aux OS  

*Sur Windows:*
```bash=
PS C:\WINDOWS\system32> netstat -a -b -n -p TCP

Active Connections

  Proto  Local Address          Foreign Address        State
 TCP    10.3.1.1:11732         10.3.1.11:22           ESTABLISHED
 [ssh.exe]
```

*Sur la VM:*
```bash=
[hugo@localhost ~]$ ss -t
State                          Recv-Q                           Send-Q                                                     Local Address:Port                                                     Peer Address:Port                           Process
ESTAB                          0                                52                                                             10.3.1.11:ssh                                                          10.3.1.1:11732
```
*Le port 22 est traduit par ssh*  

### 2. NFS
Mettez en place un petit serveur NFS sur l'une des deux VMs  

*Sur la VM faisant office de serveur NFS:*  
```bash=
[hugo@localhost ~]$ sudo dnf -y install nfs-utils
...
Complete!
[hugo@localhost ~]$ sudo vim /etc/exports
[hugo@localhost /]$ sudo mkdir /srv/shared
[hugo@localhost ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[hugo@localhost /]$ sudo firewall-cmd --add-service=nfs
success
```

*Sur la VM client:*
```bash=
[hugo@localhost ~]$ sudo dnf -y install nfs-utils
...
Complete!
[hugo@localhost ~]$ sudo mount -t nfs 10.3.1.254:/home/hugo/shared /mnt
[hugo@localhost ~]$ df -hT
...
10.3.1.254:/home/hugo/shared nfs4      6.2G  1.2G  5.1G  18% /mnt
```

*Wireshark it !*

🦈[PCAP NFS](./PCAP/tp4_nfs.pcapng)

*Le port utilisé par le serveur est donc le 2049*  

Demandez aux OS  

*Sur le serveur:*
```bash=
State                          Recv-Q                           Send-Q                                                     Local Address:Port                                                     Peer Address:Port                           Process
ESTAB                          0                                0                                                             10.3.1.254:2049                                                        10.3.1.11:702
```
*Sur le client:*
```bash=
[hugo@localhost ~]$ ss -t
State                          Recv-Q                          Send-Q                                                   Local Address:Port                                                        Peer Address:Port                           Process
ESTAB                          0                               0                                                            10.3.1.11:ssh                                                           10.3.1.254:40070
ESTAB                          0                               0                                                            10.3.1.11:iris-beep                                                     10.3.1.254:nfs
```
*Le port nfs a été traduit du numéro 2049 et iris-beep par 702*

### 3. DNS
Utilisez une commande pour effectuer une requête DNS depuis une des VMs
```bash=
[hugo@localhost ~]$ sudo tcpdump -i enp0s3 -w tp4_dns.pcap not port 22
[hugo@localhost ~]$ dig ynov.com
...
```

*On remarque ainsi que l'IP du serveur DNS sur lequel on se connecte est 8.8.8.8 sur le port 53*