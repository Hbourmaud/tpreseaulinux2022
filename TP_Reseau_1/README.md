# B2 Réseau 2022 - TP1

## I. Exploration locale en solo
### 1. Affichage d'informations sur la pile TCP/IP locale

Affichez les infos des cartes réseau de votre PC
```bash=
PS C:\WINDOWS\system32> ipconfig /all
...
Ethernet adapter Ethernet:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . :
   Description . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Physical Address. . . . . . . . . : 70-8B-CD-10-2A-D6
   DHCP Enabled. . . . . . . . . . . : No
   Autoconfiguration Enabled . . . . : Yes
...
Wireless LAN adapter Wi-Fi:

   Connection-specific DNS Suffix  . :
   Description . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 7265
   Physical Address. . . . . . . . . : 48-45-20-D6-79-6E
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
   IPv4 Address. . . . . . . . . . . : 10.33.19.88(Preferred)
...
```
Affichez votre gateway
```bash=
PS C:\WINDOWS\system32> ipconfig /all
...
Wireless LAN adapter Wi-Fi:
   ...
   Default Gateway . . . . . . . . . : 10.33.19.254
   ...
...
```

Trouvez comment afficher les informations sur une carte IP (GUI)

![info card IP](./Img/wireless_card.PNG)


**A quoi sert la gateway dans le réseau d'YNOV ?**

*A sortir du réseau Ynov pour avoir accès à d'autres réseaux comme et surtout Internet ici.*

### 2. Modifications des informations
#### A. Modification d'adresse IP (part 1)

Utilisez l'interface graphique de votre OS pour changer d'adresse IP

![Change IP](./Img/ipconfig_manual.PNG)

```bash=
PS C:\WINDOWS\system32> ipconfig /all
...
Wireless LAN adapter Wi-Fi:
   ...
   IPv4 Address. . . . . . . . . . . : 10.33.19.90(Duplicate)
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   ...
```

**Il est possible que vous perdiez l'accès internet.**

*Il est possible de perdre l'accès à internet en changeant son adresse ip manuellement car on peut nous attribuer une adresse déjà utilisée sur une autre machine sur le réseau donc l'ip se retrouvera "dupliquée" sur le réseau et donc l'accès à internet sera laissée à l'ordinateur avec l'adresse ip attribuée la plus ancienne entre les deux.*

## II. Exploration locale en duo
### 3. Modification d'adresse IP
Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :
- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
- vérifiez à l'aide de commandes que vos changements ont pris effet
- utilisez `ping` pour tester la connectivité entre les deux machines
- affichez et consultez votre table ARP

```bash=
PS C:\WINDOWS\system32> ipconfig

Windows IP Configuration

Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . :
   IPv4 Address. . . . . . . . . . . : 192.168.137.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.0.3
...
PS C:\WINDOWS\system32> ping 192.168.137.2

Pinging 192.168.137.2 with 32 bytes of data:
Reply from 192.168.137.2: bytes=32 time=1ms TTL=64
Reply from 192.168.137.2: bytes=32 time<1ms TTL=64
Reply from 192.168.137.2: bytes=32 time<1ms TTL=64
Reply from 192.168.137.2: bytes=32 time=1ms TTL=64

Ping statistics for 192.168.137.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
PS C:\WINDOWS\system32> arp -a
...
Interface: 192.168.137.1 --- 0x10
  Internet Address      Physical Address      Type
  192.168.137.2           d8-bb-c1-20-21-c0     dynamic
  192.168.0.3           ff-ff-ff-ff-ff-ff     static
  ...
```
*Les deux machines sont dans le réseaux 192.168.137. et on peut remarquer via la table ARP qu'il y a bien eu contact avec la machine 2 (192.168.137.2)*
### 4. Utilisation d'un des deux comme gateway
Pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

```bash=
ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) octets de données.
64 octets de 1.1.1.1 : icmp_seq=1 ttl=54 temps=22.1 ms
64 octets de 1.1.1.1 : icmp_seq=2 ttl=54 temps=23.3 ms
64 octets de 1.1.1.1 : icmp_seq=3 ttl=54 temps=23.8 ms
^C
--- statistiques ping 1.1.1.1 ---
3 paquets transmis, 3 reçus, 0% packet loss, time 2003ms
```

Utiliser un traceroute pour bien voir que les requêtes passent par la passerelle choisie

```bash=
traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (192.168.137.1)  0.658 ms * *
 2  * * *
 3  10.33.19.254 (10.33.19.254)  4.338 ms  4.334 ms  4.391 ms
 4  137.149.196.77.rev.sfr.net (77.196.149.137)  3.789 ms  3.784 ms  3.780 ms
 5  108.97.30.212.rev.sfr.net (212.30.97.108)  9.102 ms  9.085 ms  9.081 ms
 6  222.172.136.77.rev.sfr.net (77.136.172.222)  21.000 ms  19.750 ms  18.973 ms
 7  221.172.136.77.rev.sfr.net (77.136.172.221)  21.249 ms  21.244 ms  21.548 ms
 8  186.144.6.194.rev.sfr.net (194.6.144.186)  22.715 ms  23.690 ms  22.404 ms
 9  186.144.6.194.rev.sfr.net (194.6.144.186)  22.388 ms  21.678 ms  21.665 ms
10  72.14.194.30 (72.14.194.30)  20.045 ms  20.574 ms  20.558 ms
11  * * *
12  dns.google (8.8.8.8)  21.889 ms  21.743 ms  20.813 ms
```
*On peut remarquer le passage par la passerelle avec la ligne gateway (192.168.137.1) qui est la machine partageant internet*

### 5. Petit chat privé

Sur le PC serveur
```bash=
PS H:\netcat\netcat-1.11> .\nc.exe -l -p 8888
Bonjour
cc
```
Sur le PC client
 ```bash=
netcat                                  
Cmd line: 192.168.137.1 8888
Bonjour
cc
 ```
 
Pour aller un peu plus loin
 ```bash=
 PS H:\netcat\netcat-1.11> .\nc.exe -l -p 8888 -s 192.168.137.1
salut
cc
ca va ?
oue
 ```
*On peut choisir sur quel interface ouvrir le port avec l'option -s suivi de l'adresse de l'interface, ici la carte ethernet* 

### 6. Firewall
Autoriser les ping
```bash=
netsh advfirewall firewall set rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in new action=allow
```
*Ici on ne fait que set la règle puisque déjà existante pour ma part en allow pour accepter les pings*
Autoriser le traffic sur le port qu'utilise nc
```bash=
netsh advfirewall firewall add rule name="TCP Port 8888" dir=in action=allow protocol=TCP localport=8888
```
*Ici on créée la règle permettant d'ouvrir le port TCP 8888 puisque inexistante avant*
Communication avec netcat et firewall actif
```bash=
PS H:\netcat\netcat-1.11> .\nc.exe -l -p 8888 -s 192.168.137.1
bonjour, comment vas-tu ?
très bien merci
```
## III. Manipulations d'autres outils/protocoles côté client
### 1. DHCP
Exploration du DHCP, depuis votre PC (avec le réseau de chez moi car j'étais plus à Ynov)
*L'adresse du serveur DHCP et DNS est identique étant donnée que je suis chez moi utilisant une "box" jouant plusieurs roles à la fois*
```bash=
PS C:\WINDOWS\system32> ipconfig /all

Windows IP Configuration
...
Wireless LAN adapter Wi-Fi:
   ...
   Lease Obtained. . . . . . . . . . : 28 September 2022 13:49:17
   Lease Expires . . . . . . . . . . : 05 October 2022 13:54:20
   
   DHCP Server . . . . . . . . . . . : 10.188.0.1
   ...
```
### 2. DNS
 Trouver l'adresse IP du serveur DNS que connaît votre ordinateur
 ```bash=
PS C:\WINDOWS\system32> ipconfig /all

Windows IP Configuration
...
 Wireless LAN adapter Wi-Fi:
   ...
   DNS Servers . . . . . . . . . . . : 10.188.0.1
 ```

Utiliser, en ligne de commande l'outil nslookup pour faire des requêtes DNS à la main

Lookup :
```bash=
PS C:\WINDOWS\system32> nslookup
Default Server:  lanspeedtest.wifirst.fr
Address:  10.188.0.1

> google.com
Server:  lanspeedtest.wifirst.fr
Address:  10.188.0.1

Non-authoritative answer:
Name:    google.com
Addresses:  2a00:1450:4007:819::200e
          216.58.213.174

> ynov.com
Server:  lanspeedtest.wifirst.fr
Address:  10.188.0.1

Non-authoritative answer:
Name:    ynov.com
Addresses:  2606:4700:20::681a:ae9
          2606:4700:20::681a:be9
          2606:4700:20::ac43:4ae2
          104.26.11.233
          172.67.74.226
          104.26.10.233
```
*Grace à ces commandes, nous pouvons savoir les adresses IP sous les noms de domaines de google et ynov. On peut remarquer que ynov.com possède plusieurs adressses IP rattachées à son nom de domaine.*
*L'adresse IP à qui les requêtes sont effectuées est 10.188.0.1*
Reverse lookup :
```bash=
> 78.74.21.21
Server:  lanspeedtest.wifirst.fr
Address:  10.188.0.1

Name:    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

> 92.146.54.88
Server:  lanspeedtest.wifirst.fr
Address:  10.188.0.1

*** lanspeedtest.wifirst.fr can't find 92.146.54.88: Non-existent domain
```
*Grace à ces commandes nous pouvons connaitre le nom de domaine rattaché à une adresse IP quand celle ci existe (ou joignable). Par exemple la première apaprtient à Telia et la seconde n'a pas de nom de domaine associé.*
## IV. Wireshark
Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet

Ping:

![Ping](./Img/ping_wireshark.png)
Netcat (message envoyé: hey hey):

![Netcat](./Img/netcat_wireshark.png)
Requête DNS:

![DNS](./Img/dns_wireshark.png)