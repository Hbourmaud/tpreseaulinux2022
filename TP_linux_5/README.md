# TP5

# Sommaire
- [Software Passbolt](#Installation-de-Passbolt)
- [Database Mariadb](#Database)
- [Monitoring](#Monitoring)
- [Backup NFS](#Backup-Some-Files)
- [Réplication Database](#Réplication-de-la-base-de-donnée)

Groupe : [Brandon](https://gitlab.com/_Brante_) et [Hugo](https://gitlab.com/Hbourmaud)

Pour ce tp nous avons choisi le software Passbolt qui est un gestionnaire de mot de passe.  
Nous utiliserons uniquements des machines rocky linux 9.  


**Liste des machines :**
|  Nom     |    IP utilisée   | Description |
|---    |:-:    | :-: |
|   tp5    |   ``10.104.1.20``    | Machine principale où passbolt tourne |
|    database   | ``10.104.1.30``      | Base de données mysql|
|  backup     |   ``10.104.1.40``    | NFS pour backup |
|    proxy-tp5   | ``10.104.1.50``      | Reverse Proxy |
|    replication   |  ``10.104.1.60``     | Replication de la base de donnée| 

-> Interdire l'authentification par mot de passe une fois que les clés ssh sont mises où il faut. (notamment pour tp5)
```bash
[brante@tp5 ssh]$ cat sshd_config
[...]
PasswordAuthentication no
[...]
[brante@tp5 ssh]$ sudo service sshd restart
Redirecting to /bin/systemctl restart sshd.service
[brante@tp5 ssh]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2022-12-01 12:02:51 CET; 11s ago
[...]
```

---

### Installation de Passbolt
-> installation des logiciels
```bash
[hugo@tp5 ~]$ sudo dnf install git -y
[...]
Complete!
[brante@tp5 ~]$ sudo dnf install passbolt-ce-server
[...]
Installing:
 passbolt-ce-server                    noarch          3.8.3-1                        passbolt-server           10 M
[...]
Install  55 Packages

Total download size: 38 M
Installed size: 130 M
[...]
Complete!
```
Nginx, PHP et quasi toutes les dépendances ont aussi été installé.

**Configuration de passbolt**

-> Création du certificat SSL manuellement
```bash
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
...
[hugo@tp5 ~]$ sudo mv server.crt web.tp2.linux.crt
[hugo@tp5 ~]$ sudo mv server.key web.tp2.linux.key
[hugo@tp5 ~]$ sudo mv web.tp2.linux.crt /etc/pki/tls/certs/
[hugo@tp5 ~]$ sudo mv web.tp2.linux.key /etc/pki/tls/private/
```

-> lancement passbolt-configureoutil
```bash
[brante@tp5 ~]$ sudo /usr/local/bin/passbolt-configure
Do you want to install a local mariadb server on this machine?
1) yes
2) no
#? 2 
[...]
Hostname:10.104.1.20
[...]
Setting up SSL...
    Do you want to setup a SSL certificate and enable HTTPS now?
1) manual
2) auto
3) none
#? 1
Enter the path to the SSL certificate: /etc/pki/tls/certs/tp5.crt 
Enter the path to the SSL privkey: /etc/pki/tls/private/tp5.key
[...]
```
L'installeur a ouvert les services nécessaires tous seul (http https).  
Nous avons au préalable créer le certificat SSL et sa clé donc nous choisissons donc on choisi la configuration manuelle de la partie SSL de l'installeur.  
Il a configuré nginx tout seul mais la conf n'est pas compliqué. Cela ressemble à la configuration qu'on a déjà fait lors des tps précédent (sauf le php XD ).  
Nous n'avons pas accepté que le logiciel nous crée directement la base de données (en local) puisque nous voulons créer une machine séparée pour gérer notre DB.  

Nous pouvons à présent accèder à la page web en https à https://10.104.1.20 et commencer la configuration de Passbolt via l'interface graphique intégrée.  
La prochaine étape nous demande une base de donnée.

---

### Database

On commence par créer la base de données :  

-> Sécurisation de l'accès à la machine de la base de données en bloquant toutes les requêtes ssh. (sauf la machine tp5 et la machine hôte)
```bash
hugo@database ~]$ sudo firewall-cmd --zone=block --change-interface=enp0s8 --permanent
success
[hugo@database ~]$ sudo firewall-cmd --permanent --new-zone=access
success
[hugo@database ~]$ sudo firewall-cmd --zone=access --add-source=10.104.1.20 --permanent
success
[hugo@database ~]$ sudo firewall-cmd --zone=access --add-source=10.104.1.1 --permanent
success
[hugo@database ~]$ sudo firewall-cmd --zone=access --add-port=3306/tcp --permanent
success
[hugo@database ~]$ sudo firewall-cmd --zone=block --add-port=22/tcp --permanent
success
[hugo@database ~]$ sudo firewall-cmd --reload
success
```
-> Installation de mariadb
```bash
[hugo@database ~]$ sudo dnf install mariadb-server -y
[...]
Complete!
[hugo@database ~]$ sudo systemctl enable mariadb
[...]
[hugo@database ~]$ sudo systemctl start mariadb
[hugo@database ~]$ sudo mysql_secure_installation
[...]
Thanks for using MariaDB!
[hugo@database ~]$ ss -alnpt
State      Recv-Q      Send-Q           Local Address:Port           Peer Address:Port     Process
LISTEN     0           80                           *:3306                      *:*
[hugo@database ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[hugo@database ~]$ sudo firewall-cmd --reload
success
[hugo@database ~]$ sudo mysql -u root -p
[...]
MariaDB [(none)]> create user 'passbolt'@'10.104.1.20' identified by 'XXXXX';
Query OK, 0 rows affected (0.015 sec)

MariaDB [(none)]> create database passbolt character set utf8mb4 collate utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> grant all privileges on passbolt.* to 'passbolt'@'10.104.1.20';
Query OK, 0 rows affected (0.016 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.001 sec)
```
On rentre les infos de la base de données sur l'interface web puis on continue la configuration de passbolt.

---

On crée notre administrateur et on le configure avec les instructions de Passbolt.  

Voila. On a maintenant un gestionnaire de mot de passe qui fonctionne.

---

Concernant le **serveur SMTP**, nous n'avons pas eu le besoin de monter une nouvelle machine. Le service open-source propose déjà des solutions. Nous avons utilisé l'une d'entre-elles.  
Durant la configuration, tous les tests de vérification sont passés mais lors de la création de compte, l'utilisateur **ne reçoit pas de mail de la part de Passbolt**.  
Tout est conforme au niveau de la configuration.

Nous ne pouvons donc pas créer de nouvel utilisateur sur notre solution.  
Tout reste fonctionnel malgré ce petit problème via l'utilisateur créée par défaut.

---

## Amélioration de la solution

### Monitoring

C'est cool de savoir si nos machines sont en galères pour quelconque raison donc on mets en place un monitoring sur les machines via ``NetData`` qui va pouvoir nous notifier sur discord des états des machines.

Pour ça on installe ce qui faut:
```bash
[hugo@tp5 ~]$ sudo dnf install epel-release -y
...
Complete!
[hugo@tp5 ~]$ sudo dnf install wget -y
...
Complete!
wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
...
Complete!
...
[hugo@tp5 ~]$ sudo systemctl start netdata
[hugo@tp5 ~]$ sudo systemctl enable netdata
[hugo@tp5 ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[hugo@tp5 ~]$ sudo firewall-cmd --reload
success
```
On configure netdata pour qu'il envoie des alertes sur discord.
```bash
[hugo@tp5 ~]$ cd /etc/netdata
[hugo@tp5 ~]$ sudo ./edit-config health_alarm_notify.conf
[hugo@tp5 ~]$ sudo systemctl restart netdata
```
On peut tester si ça marche avec ``stress`` par exemple.  
On répète le processus autant de fois que nécessaire pour chaque machine.

---

### Backup Some Files

On va mettre en place une nouvelle machine `backup` qui sera un serveur nfs pour pouvoir sauvegarder les fichiers importants que l'on souhaite dessus.  
Sur la machine tp5, où passbolt tourne, on veut backup quelques fichiers. (indiqué par la doc passbolt)  
On écrit donc un script bash pour cela :
```bash=
[hugo@tp5 ~]$ sudo dnf install zip -y
[...]
Complete!
[hugo@tp5 ~]$ sudo vim /srv/tp5_backup.sh
[hugo@tp5 ~]$ cat /srv/tp5_backup.sh
#!/bin/bash
#Script for backup files for passbolt

datestr=(`date +%y%m%d%H%M%S`)
name_file="passbolt_"$datestr""
mkdir -p /srv/backup/
zip -q -r /srv/backup/"$name_file".zip /etc/passbolt/gpg /etc/passbolt/passbolt.php
```
Et on gère le service, timer et son exécution :
```bash=
[hugo@tp5 ~]$ sudo useradd backup -m -d /srv/backup/ -s /usr/bin/nologin
[hugo@tp5 ~]$ sudo chown backup /srv/backup/
[hugo@tp5 ~]$ sudo chown backup /srv/tp5_backup.sh
[hugo@tp5 ~]$ sudo usermod -a -G nginx backup
[hugo@tp5 ~]$ sudo systemctl daemon-reload
[hugo@tp5 ~]$ sudo -u backup sh /srv/tp5_backup.sh
[hugo@tp5 ~]$ ls /srv/backup/
passbolt_221211150908.zip
[hugo@tp5 ~]$ sudo vim /etc/systemd/system/backup.service
[hugo@tp5 ~]$ cat /etc/systemd/system/backup.service
[Unit]
Description=Execute tp5_backup for make backup of some passbolt files

[Service]
ExecStart=/srv/tp5_backup.sh
Type=oneshot
User=backup

[Install]
WantedBy=multi-user.target
[hugo@tp5 ~]$ sudo systemctl start backup
[hugo@tp5 ~]$ sudo vim /etc/systemd/system/backup.timer
[hugo@tp5 ~]$ cat /etc/systemd/system/backup.timer
[Unit]
Description=Run service backup

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
[hugo@tp5 ~]$ sudo systemctl daemon-reload
[hugo@tp5 ~]$ sudo systemctl start backup.timer
[hugo@tp5 ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[hugo@tp5 ~]$ sudo systemctl list-timers
NEXT                        LEFT         LAST                        PASSED       UNIT                         ACTIVAT>
...
Mon 2022-12-12 04:00:00 CET 12h left     n/a                         n/a          backup.timer                 backup.>
...
```
Sur le serveur backup (nfs) :
```bash=
[hugo@backup ~]$ sudo mkdir -p /srv/nfs_shares/tp5.linux/
[hugo@backup ~]$ sudo dnf install nfs-utils -y
...
Complete!
[hugo@backup ~]$ sudo vim /etc/exports
[hugo@backup ~]$ cat /etc/exports
/srv/nfs_shares/tp5.linux/ 10.104.1.20(rw,no_subtree_check,sync)
[hugo@backup ~]$ sudo systemctl start nfs-server
[hugo@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --zone=access --permanent
success
[hugo@backup ~]$ sudo firewall-cmd --reload
success
```
Sur tp5 :
```bash=
[hugo@tp5 ~]$ sudo dnf install nfs-utils -y
...
Complete!
[hugo@tp5 ~]$ sudo mount -t nfs 10.104.1.40:/srv/nfs_shares/tp5.linux/ /srv/backup/
[hugo@tp5 ~]$ df -h
Filesystem                                 Size  Used Avail Use% Mounted on
...
10.104.1.40:/srv/nfs_shares/tp5.linux  6.2G  1.7G  4.6G  27% /srv/backup
[hugo@tp5 ~]$ sudo vim /etc/fstab
[hugo@tp5 ~]$ cat /etc/fstab
...
10.104.1.40:/srv/nfs_shares/tp5.linux/ /srv/backup nfs4 defaults 0 0
[hugo@tp5 ~]$ sudo -u backup sh /srv/tp5_backup.sh
```
Le fichier est bien sur le server nfs : 
```bash=
[hugo@backup ~]$ ls /srv/nfs_shares/tp5.linux/
passbolt_221211150908.zip
```

On veut aussi backup la database donc on fait pareil sur le serveur database en dumpant la base et en changeant quelque petits trucs :  
On créée un user qu va permettre de dump la base de donnée :
```bash=
[hugo@database ~]$ sudo mysql -u root -p
Enter password:
...
MariaDB [(none)]> CREATE USER 'dumpuser'@'localhost' IDENTIFIED BY '1234';
Query OK, 0 rows affected (0.007 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON passbolt.* TO 'dumpuser'@'localhost';
Query OK, 0 rows affected (0.005 sec)

MariaDB [(none)]> FLUSH privileges;
Query OK, 0 rows affected (0.002 sec)
```
Le script bash change un peu :
```bash=
[hugo@database ~]$ sudo vim /srv/tp5_db_dump.sh
[hugo@database ~]$ sudo cat /srv/tp5_db_dump.sh
#!/bin/bash
# This script dumps a database and compress in .zip to possibly restore it after in case of needed.
# Saves Folder at /srv/db_dumps/

db_user="dumpuser"
source /srv/db_passwd
db_name="passbolt"
db_ip="localhost"
datestr=(`date +%y%m%d%H%M%S`)
name_file="db_passbolt_"$datestr""
mkdir -p /srv/db_dumps/
mysqldump -u $db_user -h $db_ip -p$db_passwd $db_name > /srv/db_dumps/"$name_file".sql
cd /srv/db_dumps/
zip -q /srv/db_dumps/"$name_file".zip "$name_file".sql
rm /srv/db_dumps/"$name_file".sql

[hugo@database ~]$ sudo vim /srv/db_passwd
```
On créer un fichier db_passwd où l'on stock la variable db_passwd contenant le mot de passe de l'user de la database.  
On créer un user spécifique appelé db_dumps sur le même modèle que la backup de tp5 ainsi qu'un service et son timer.
```bash=
[hugo@database ~]$ sudo ls /srv/db_dumps/
db_passbolt_221211162836.zip  db_passbolt_221211163258.zip
```
On souhaite mettre le fichier sur le server nfs donc on mets en place la même chose que pour la machine tp5 et le server nfs avec cette machine. (Les étapes sont quasi identiques.)

---

### Réplication de la base de donnée

On a créer une nouvelle machine `replication` qui sera notre serveur slave dans le processus master-slave pour la réplication de la database.  
On modifie d'abord la configuration de mariadb sur la machine database:
```bash=
[hugo@database ~]$ sudo vim /etc/my.cnf
[hugo@database ~]$ sudo cat /etc/my.cnf
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
[mariadb]
log-bin
server-id=1
log-basename=master1
binlog-format=mixed
skip-networking=0
bind-address=0.0.0.0
[hugo@database ~]$ sudo systemctl restart mariadb
[hugo@database ~]$ sudo mysql -u root -p
...
MariaDB [(none)]> create user 'replication'@'10.104.1.60' identified by '1234';
Query OK, 0 rows affected (0.007 sec)

MariaDB [(none)]> grant replication slave on *.* to 'replication'@'10.104.1.60';
Query OK, 0 rows affected (0.006 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> show master status;
+--------------------+----------+--------------+------------------+
| File               | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+--------------------+----------+--------------+------------------+
| master1-bin.000002 |      909 |              |                  |
+--------------------+----------+--------------+------------------+
1 row in set (0.000 sec)
```
Sur la machine replication, on installe et configure mariadb: 
```bash=
[hugo@replication ~]$ sudo dnf install mariadb-server -y
[...]
Complete!
[hugo@replication ~]$ sudo systemctl start mariadb
[hugo@replication ~]$ sudo systemctl enable mariadb
[hugo@replication ~]$ sudo mysql_secure_installation
[...]
[hugo@replication ~]$ sudo vim /etc/my.cnf
[hugo@replication ~]$ cat /etc/my.cnf
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d

[mariadb]
bind-address=0.0.0.0
server-id=2
log-bin
log-basename=slave1
binlog-format=mixed
skip-networking=0
[hugo@replication ~]$ sudo systemctl restart mariadb
[hugo@replication ~]$ sudo mysql -u root -p
[...]
MariaDB [(none)]> stop slave;
Query OK, 0 rows affected, 1 warning (0.000 sec)

MariaDB [(none)]> change master to master_host = '10.104.1.30', master_user = 'replication', master_pass
word = '1234', master_log_file = 'master1-bin.000002', master_log_pos = 909;
Query OK, 0 rows affected (0.026 sec)

MariaDB [(none)]> start slave;
Query OK, 0 rows affected (0.002 sec)
```
On peut tester si cela marche correctement en interragisant avec la database et constater sur la machine replication.

---
