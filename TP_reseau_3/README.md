# TP3 : On va router des trucs
## I. ARP
### 1. Echange ARP
Générer des requêtes ARP  
*Depuis John:*  
```bash=
[hugo@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:5a:fe:68 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe5a:fe68/64 scope link
       valid_lft forever preferred_lft forever
[hugo@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.543 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.609 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.579 ms
^C
--- 10.3.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2055ms
rtt min/avg/max/mdev = 0.543/0.577/0.609/0.027 ms
[hugo@localhost ~]$ ip n s
10.3.1.12 dev enp0s3 lladdr 08:00:27:8a:69:48 REACHABLE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 REACHABLE
```
*Depuis Marcel:*  
```bash=
[hugo@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8a:69:48 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe8a:6948/64 scope link
       valid_lft forever preferred_lft forever
[hugo@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.526 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.650 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.478 ms
^C
--- 10.3.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2054ms
rtt min/avg/max/mdev = 0.478/0.551/0.650/0.072 ms
[hugo@localhost ~]$ ip n s
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 REACHABLE
10.3.1.11 dev enp0s3 lladdr 08:00:27:5a:fe:68 REACHABLE
```

*Adresse MAC de John : 08:00:27:5a:fe:68*  
*Adresse MAC de Marcel : 08:00:27:8a:69:48*

### 2. Analyse de trames
Analyse de trames  
*Sur john et marcel avec quelques infos qui différe :*
```bash=
[hugo@localhost ~]$ sudo ip -s -s neigh flush all
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 ref 1 used 0/0/1 probes 4 DELAY

*** Round 1, deleting 1 entries ***
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 ref 1 used 0/0/0 probes 4 REACHABLE

*** Round 2, deleting 1 entries ***
10.3.1.1 dev enp0s3  ref 1 used 0/60/0 probes 4 INCOMPLETE

*** Round 3, deleting 1 entries ***
10.3.1.1 dev enp0s3  ref 1 used 0/0/0 probes 4 INCOMPLETE

*** Round 4, deleting 1 entries ***
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 ref 1 used 0/0/0 probes 4 REACHABLE

*** Round 5, deleting 1 entries ***
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 ref 1 used 0/0/0 probes 4 REACHABLE

*** Round 6, deleting 1 entries ***
10.3.1.1 dev enp0s3  ref 1 used 0/60/0 probes 4 INCOMPLETE

*** Round 7, deleting 1 entries ***
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 ref 1 used 0/0/0 probes 4 REACHABLE

*** Round 8, deleting 1 entries ***
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:05 ref 1 used 0/0/0 probes 4 REACHABLE

*** Round 9, deleting 1 entries ***
10.3.1.1 dev enp0s3  ref 1 used 0/60/0 probes 4 INCOMPLETE

*** Round 10, deleting 1 entries ***
*** Flush not complete bailing out after 10 rounds
```
*Sur John:*
```bash=
[hugo@localhost ~]$ sudo tcpdump -i enp0s3 -c 10 -w tp2_arp.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```
*Sur Marcel:*
```bash=
[hugo@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.904 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.623 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.622 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.706 ms
64 bytes from 10.3.1.11: icmp_seq=5 ttl=64 time=0.611 ms
64 bytes from 10.3.1.11: icmp_seq=6 ttl=64 time=0.619 ms
^C
--- 10.3.1.11 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5094ms
rtt min/avg/max/mdev = 0.611/0.680/0.904/0.104 ms
```
🦈[PCAP ARP](./PCAP/tp2_arp.pcapng)

## II. Routage
### 1. Mise en place du routage
Activer le routage sur le noeud router  

*Sur Router:*
```bash=
[hugo@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[hugo@localhost ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8
[hugo@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[hugo@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping  

*Sur John:*
```bash=
[hugo@localhost ~]$ sudo vim /etc/sysconfig/network-scripts/route-enp0s3
[sudo] password for hugo:
[hugo@localhost ~]$ cat /etc/sysconfig/network-scripts/route-enp0s3
10.3.2.0/24 via 10.3.1.254 dev enp0s3
[hugo@localhost ~]$ sudo systemctl restart NetworkManager
[hugo@localhost ~]$ ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100
```
*Pour John on souhaite accèder au réseau 10.3.2.0/24 via la passerelle 10.3.1.254 (le router)*  

*Sur Marcel:*
```bash=
[hugo@localhost ~]$ sudo vim /etc/sysconfig/network-scripts/route-enp0s3
[sudo] password for hugo:
[hugo@localhost ~]$ cat /etc/sysconfig/network-scripts/route-enp0s3
10.3.1.0/24 via 10.3.2.254 dev enp0s3
[hugo@localhost ~]$ sudo systemctl restart NetworkManager
[hugo@localhost ~]$ ip r s
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 100
10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100
```
*Pour Marcel on souhaite accèder au réseau 10.3.1.0/24 via la passerelle 10.3.2.254 (le router)*  

*Ping de Marcel depuis John:*
```bash=
[hugo@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.29 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.35 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.43 ms
```
### 2. Analyse de trames
Analyse des échanges ARP  

*Pour vider les tables ARP. Sur les trois machines:*
```bash=
[hugo@localhost ~]$ sudo ip neigh flush all
```
*Sur John:*
```bash=
[hugo@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=2.65 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.09 ms
[hugo@localhost ~]$ ip n s
10.3.1.254 dev enp0s3 lladdr 08:00:27:01:0d:3f STALE
```
*Sur Marcel:*
```bash=
[hugo@localhost ~]$ ip n s
10.3.2.254 dev enp0s3 lladdr 08:00:27:51:99:24 STALE
```
*Sur Router:*
```bash=
[hugo@localhost ~]$ ip n s
10.3.2.12 dev enp0s8 lladdr 08:00:27:8a:69:48 STALE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:09 STALE
10.3.1.11 dev enp0s3 lladdr 08:00:27:5a:fe:68 STALE
```
*Capture des trames sur marcel:*
```bash=
[hugo@localhost ~]$ sudo tcpdump -i enp0s3 -c 20 -w tp2_routage_marcel.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
20 packets captured
20 packets received by filter
0 packets dropped by kernel
```
*On peut remarquer que les échanges ARP qui se sont produits sont ceux de John vers la passerelle (le router) et de Marcel vers la passerelle donc leurs tables ARP ne contient que les informations de la machine Router car c'est par cette machine que les deux machines ont besoin de passer pour communiquer entre elles.*  


| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `router` `08:00:27:51:99:24` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `marcel` `08:00:27:8a:69:48`                       | x              | `router` `08:00:27:51:99:24`    |
| 3   | Requête ARP         | x       | `marcel` `08:00:27:8a:69:48`                     |   x             |  `router` `08:00:27:51:99:24`                          |
| 4     | Réponse ARP | x         | `router` `08:00:27:51:99:24`                       | x              | `marcel` `08:00:27:8a:69:48`    || ?     | Ping        | ?         | ?                       | ?              | ?                          |
| 6     | Ping        | 10.3.2.254         | `router` `08:00:27:51:99:24`                       | 10.3.2.12              |   `marcel` `08:00:27:8a:69:48`                        |
| 7     | Pong        | 10.3.2.12         |   `marcel` `08:00:27:8a:69:48`                     | 10.3.2.254              |  `router` `08:00:27:51:99:24`                         |

🦈[PCAP Routage](./PCAP/tp2_routage_marcel.pcapng)

### 3. Accès internet
Donnez un accès internet à vos machines  
Ajout de la route par défaut:  
*Sur John:*
```bash=
[hugo@localhost ~]$ sudo vim /etc/sysconfig/network
[sudo] password for hugo:
[hugo@localhost ~]$ cat /etc/sysconfig/network
GATEWAY=10.3.1.254
[hugo@localhost ~]$ sudo systemctl restart NetworkManager
[hugo@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=12.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=11.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=118 time=12.2 ms
```
*Sur Marcel:*
```bash=
[hugo@localhost ~]$ sudo vim /etc/sysconfig/network
[sudo] password for hugo:
[hugo@localhost ~]$ cat /etc/sysconfig/network
GATEWAY=10.3.2.254
[hugo@localhost ~]$ sudo systemctl restart NetworkManager
[hugo@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=12.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=11.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=118 time=12.2 ms
```

*On ajoute une gateway pour chacune des machines pour pouvoir accèder aux autres réseaux qui ne sont pas accesible via les routes statiques configurées*  

Utilisation d'un serveur DNS:  
*Sur John et Marcel :*
```bash=
[hugo@localhost ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
[hugo@localhost ~]$ dig

...
;; ANSWER SECTION:
.                       86683   IN      NS      e.root-servers.net.
.                       86683   IN      NS      m.root-servers.net.
.                       86683   IN      NS      b.root-servers.net.
...

;; Query time: 12 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Tue Oct 11 14:41:34 CEST 2022
;; MSG SIZE  rcvd: 239

[hugo@localhost ~]$ ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=1 ttl=118 time=11.3 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=2 ttl=118 time=11.4 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=3 ttl=118 time=11.9 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=4 ttl=118 time=11.0 ms
```
*On remarque qu'il y a déjà des adresses de serveur DNS générée automatiquement par NetworkManager comme l'adresse 1.1.1.1 dans le fichier resolv.conf. Si jamais ce n'est pas le cas il faudrait en rajouter au moins une.*  

Analyse de trames  

| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |
|-------|------------|--------------------|-------------------------|----------------|-----------------|
| 1     | ping       | `john` `10.3.1.11` | `john` `08:00:27:5a:fe:68` | `8.8.8.8`      | `router` `08:00:27:01:0d:3f`               |
| 2     | pong       | `8.8.8.8`                | `router` `08:00:27:01:0d:3f`                     | `john` `10.3.1.11`          | `john` `08:00:27:5a:fe:68`             |

*On remarque ainsi que pour contacter "internet" (d'autres réseaux) la machine john passe bien par le router.*  

🦈[PCAP Routage Internet](./PCAP/tp2_routage_internet.pcapng)

## III. DHCP
### 1. Mise en place du serveur DHCP
*Sur John:*
```bash=
[hugo@localhost ~]$ sudo dnf install dhcp-server
[sudo] password for hugo:

..

Complete!

[hugo@localhost ~]$ sudo vim /etc/dhcp/dhcpd.conf
[hugo@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for hugo:
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.0 {
  range 10.3.1.12 10.3.1.200;
}
[hugo@localhost ~]$ sudo firewall-cmd --permanent --add-port=67/udp
success
[hugo@localhost ~]$ sudo systemctl enable dhcpd --now
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
```

*On installe dhcp server et ensuite on configure le serveur en indiquant le réseau ainsi que son masque et l'intervalle d'adresses IP disponibles à distribuer. On configure le firewall pour autoriser la communication via le port 67*  

*Sur Bob:*
```bash=
[hugo@localhost ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:22:65:cc brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a00:27ff:fe22:65cc/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[hugo@localhost ~]$ sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
[sudo] password for hugo:
[hugo@localhost ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=System enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
[hugo@localhost ~]$ sudo nmcli con up "System enp0s3"
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
[hugo@localhost ~]$ sudo systemctl restart NetworkManager
[hugo@localhost ~]$ ip a
...
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:22:65:cc brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 607sec preferred_lft 607sec
    inet6 fe80::a00:27ff:fe22:65cc/64 scope link
       valid_lft forever preferred_lft forever
```

*On configure l'interface (ici enp0s3) pour pouvoir se faire attribuer une adresse via DHCP*  

Améliorer la configuration du DHCP  

*Sur John:*
```bash=
[hugo@localhost ~]$ sudo vim /etc/dhcp/dhcpd.conf
[sudo] password for hugo:
[hugo@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.0 {
  range 10.3.1.12 10.3.1.200;
  option routers 10.3.1.254;
  option domain-name-servers 1.1.1.1;
}
[hugo@localhost ~]$ sudo systemctl restart dhcpd
```

*On rajoute l'adresse du router comme route par défaut ainsi que l'adresse d'un serveur DNS (ici 1.1.1.1)*  

*Sur Bob:*  
```bash=
[hugo@localhost ~]$ ip a
...
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:22:65:cc brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 725sec preferred_lft 725sec
    inet6 fe80::a00:27ff:fe22:65cc/64 scope link
       valid_lft forever preferred_lft forever
[hugo@localhost ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.475 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=0.677 ms
[hugo@localhost ~]$ ip r s
default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.12 metric 100
[hugo@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.56 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.26 ms
[hugo@localhost ~]$ dig ynov.com

; <<>> DiG 9.16.23-RH <<>> ynov.com
...
;; ANSWER SECTION:
ynov.com.               192     IN      A       172.67.74.226
ynov.com.               192     IN      A       104.26.11.233
ynov.com.               192     IN      A       104.26.10.233

;; Query time: 13 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
...
[hugo@localhost ~]$ ping ynov.com
PING ynov.com (104.26.10.233) 56(84) bytes of data.
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=1 ttl=57 time=11.0 ms
64 bytes from 104.26.10.233 (104.26.10.233): icmp_seq=2 ttl=57 time=11.7 ms
```

*On remarque qu'il y a bien une route via 10.3.1.254 ainsi que le serveur DNS 1.1.1.1 utilisée pour la résolution de nom de domaine*

### 2. Analyse de trames
Analyse de trames  

*On peut via Wireshark remarquer qu'il y a bien un échange entre Bob et le serveur DHCP (donc John avec l'adresse IP 10.3.1.11)*  

🦈[PCAP DHCP](./PCAP/tp2_dhcp.pcapng)