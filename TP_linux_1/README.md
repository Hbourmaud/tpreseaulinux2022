# TP1 : (re)Familiarisation avec un système GNU/Linux
### 0. Préparation de la machine
**Setup de deux machines Rocky Linux configurées de façon basique.  
Un accès internet (via la carte NAT)**
```bash=
[hugo@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=23.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=31.5 ms
```
**Un accès à un réseau local**
```bash=
[hugo@localhost ~]$ sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s8
[hugo@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0
[hugo@localhost ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.888 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.629 ms
```
**Les machines doivent avoir un nom**
```bash=
[hugo@node1 ~]$ sudo vim /etc/hostname
[hugo@node1 ~]$ cat /etc/hostname
node1.tp1.b2
[hugo@node1 ~]$ hostname
node1.tp1.b2
```
**Utiliser 1.1.1.1 comme serveur DNS**
```bash=
[hugo@node1 ~]$ sudo vim /etc/resolv.conf
[sudo] password for hugo:
[hugo@node1 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1
[hugo@node1 ~]$ dig ynov.com
...
;; ANSWER SECTION:
ynov.com.               300     IN      A       172.67.74.226
ynov.com.               300     IN      A       104.26.10.233
ynov.com.               300     IN      A       104.26.11.233
...
;; Query time: 31 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
...
```
**Les machines doivent pouvoir se joindre par leurs noms respectifs**
```bash=
[hugo@node1 ~]$ sudo vim /etc/hosts
[hugo@node1 ~]$ cat /etc/hosts
...
10.101.1.12 node2.tp1.b2
[hugo@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.715 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.740 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.546 ms
```
**Le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
```bash=
[hugo@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

On réalise la même chose avec la machine node2, en changeant :
- IPADDR=10.101.1.12 dans ifcfg-enp0s8
- L'adresse que l'on ping est celui de l'autre machine
- l'hostname qui est node2.tp1.b2 dans /etc/hostname

## I. Utilisateurs
### 1. Création et configuration
**Ajouter un utilisateur à la machine, qui sera dédié à son administration**
```bash=
[hugo@node1 ~]$ sudo useradd toto -m -d /home/toto -s /bin/bash
[hugo@node1 ~]$ cat /etc/passwd
...
toto:x:1001:1001::/home/toto:/bin/bash
```
L'option -d permet d'indiquer le chemin du répertoir home et -s pour le shell.  
**Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo**
```bash=
[hugo@node1 ~]$ sudo groupadd admins
[hugo@node1 ~]$ sudo visudo /etc/sudoers
[hugo@node1 ~]$ sudo cat /etc/sudoers
...
%admins ALL=(ALL)       ALL
```
**Ajouter votre utilisateur à ce groupe admins**
```bash=
[hugo@node1 ~]$ sudo usermod -aG admins toto
[hugo@node1 ~]$ su toto
Password:
[toto@node1 hugo]$ sudo cat /etc/sudoers
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
...
```

### 2. SSH
**Pour cela...**
```bash=
[toto@node1 ~]$ mkdir .ssh
[toto@node1 ~]$ sudo vim authorized_keys
[toto@node1 ~]$ exit
PS C:\Users\User> ssh hugo@10.101.1.11
Last login: Mon Nov 14 16:54:56 2022 from 10.101.1.1
[hugo@node1 ~]$
```
La connexion est directe sans demande de mot de passe.
## II. Partitionnement
### 2. Partitionnement
Utilisez LVM pour...
```bash=
[toto@node1 /]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
...
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
...
[toto@node1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[toto@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[toto@node1 ~]$ sudo pvs
...
  PV         VG   Fmt  Attr PSize  PFree
  /dev/sdb   data lvm2 a--  <3.00g   <3.00g
  /dev/sdc   data lvm2 a--  <3.00g   <3.00g
```
Les disques ajoutées sont nommées sdb et sdc ici.  
**Agréger les deux disques en un seul volume group**
```bash=
[toto@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[toto@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
[toto@node1 ~]$ sudo vgs
...
  VG   #PV #LV #SN Attr   VSize VFree
  data   2   0   0 wz--n- 5.99g 5.99g
```
Le nom du volume group est data.  
**Créer 3 logical volumes de 1 Go chacun**
```bash=
[toto@node1 ~]$ sudo lvcreate -L 1G data -n small_data
  Logical volume "small_data" created.
[toto@node1 ~]$ sudo lvcreate -L 1G data -n small_data_again
  Logical volume "small_data_again" created.
[toto@node1 ~]$ sudo lvcreate -L 1G data -n last_small_data_again
  Logical volume "last_small_data_again" created.
[toto@node1 ~]$ sudo lvs
...
  LV                    VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  last_small_data_again data -wi-a----- 1.00g
  small_data            data -wi-a----- 1.00g
  small_data_again      data -wi-a----- 1.00g
```
**Formater ces partitions en ext4**
```bash=
[toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/small_data
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 262144 4k blocks and 65536 inodes
...
Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
[toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/small_data_again
...
Writing superblocks and filesystem accounting information: done
[toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/last_small_data_again
...
Writing superblocks and filesystem accounting information: done
```
**Monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3.**
```bash=
[toto@node1 ~]$ sudo mkdir /mnt/part1
[toto@node1 ~]$ sudo mkdir /mnt/part2
[toto@node1 ~]$ sudo mkdir /mnt/part3
[toto@node1 ~]$ sudo mount /dev/data/small_data /mnt/part1
[toto@node1 ~]$ sudo mount /dev/data/small_data_again /mnt/part2
[toto@node1 ~]$ sudo mount /dev/data/last_small_data_again /mnt/part3
[toto@node1 ~]$ df -h
Filesystem                              Size  Used Avail Use% Mounted on
...
/dev/mapper/data-small_data             974M   24K  907M   1% /mnt/part1
/dev/mapper/data-small_data_again       974M   24K  907M   1% /mnt/part2
/dev/mapper/data-last_small_data_again  974M   24K  907M   1% /mnt/part3
```
**Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.**
```bash=
[toto@node1 ~]$ sudo vim /etc/fstab
[toto@node1 ~]$ cat /etc/fstab
...
/dev/data/small_data /mnt/part1 ext4 defaults 0 0
/dev/data/small_data_again /mnt/part2 ext4 defaults 0 0
/dev/data/last_small_data_again /mnt/part3 ext4 defaults 0 0
[toto@node1 ~]$ sudo umount /mnt/part1
[toto@node1 ~]$ sudo umount /mnt/part2
[toto@node1 ~]$ sudo umount /mnt/part3
[toto@node1 ~]$ sudo mount -av
...
/mnt/part1               : successfully mounted
...
/mnt/part2               : successfully mounted
...
/mnt/part3               : successfully mounted
```
## III. Gestion de services
### 1. Interaction avec un service existant
**Assurez-vous que...**
**L'unité est démarrée**
```bash=
[toto@node1 ~]$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-11-14 17:10:35 CET; 41min ago
...
```
**L'unitée est activée**
```bash=
[toto@node1 ~]$ sudo systemctl is-enabled firewalld
enabled
```
### 2. Création de service
#### A. Unité simpliste
**Créer un fichier qui définit une unité de service**
```bash=
[toto@node1 ~]$ sudo vim /etc/systemd/system/web.service
[toto@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
[toto@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
Success
[toto@node1 ~]$ sudo firewall-cmd --reload
Success
```
**Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**
```bash=
[toto@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
...
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
#### B. Modification de l'unité
**Préparez l'environnement pour exécuter le mini serveur web Python**
```bash=
[toto@node1 ~]$ sudo useradd web
[toto@node1 ~]$ sudo mkdir /var/www/meow/
[toto@node1 ~]$ sudo vim /var/www/meow/test
[toto@node1 ~]$ sudo chown web /var/www/meow
[toto@node1 ~]$ sudo chown web /var/www/meow/test
[toto@node1 ~]$ ls -la /var/www/meow/
total 4
drwxr-xr-x. 2 web  root 18 Nov 14 18:26 .
drwxr-xr-x. 3 root root 18 Nov 14 18:26 ..
-rw-r--r--. 1 web  root  5 Nov 14 18:26 test
```
**Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses**
```bash=
[toto@node1 ~]$ sudo vim /etc/systemd/system/web.service
[toto@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow/

[Install]
WantedBy=multi-user.target
```
**Vérifiez le bon fonctionnement avec une commande curl**
```bash=
[toto@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="test">test</a></li>
</ul>
<hr>
</body>
</html>
```

Le contenu de la page a changé et on remarque bien notre fichier test dans la list html.
