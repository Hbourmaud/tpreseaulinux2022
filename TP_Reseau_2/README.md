# TP2 : Ethernet, IP, et ARP
## I. Setup IP
Mettez en place une configuration réseau fonctionnelle entre les deux machines

Le masque choisi est /26 car 62 adresses sont disponibles alors qu'en /27 il y en a seulement 30 adresses

Les Ip choisies : 192.168.0.1 /26 et 192.168.0.2 /26

Adresse réseau : 192.168.0.0 /26

Adresse broadcast : 192.168.0.63 /26

```bash=
PS C:\WINDOWS\system32> netsh interface ipv4 set address name="Ethernet" static 192.168.0.1 255.255.255.192

PS C:\WINDOWS\system32> ipconfig                                                                                        
Windows IP Configuration

Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . :
   IPv4 Address. . . . . . . . . . . : 192.168.0.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.192
   Default Gateway . . . . . . . . . : 
```
Prouvez que la connexion est fonctionnelle entre les deux machines
```bash=
PS C:\WINDOWS\system32> Ping 192.168.0.2
Pinging 192.168.0.2 with 32 bytes of data:
Reply from 192.168.0.2: bytes=32 time<1ms TTL=64
Reply from 192.168.0.2: bytes=32 time<1ms TTL=64

Ping statistics for 192.168.0.2:
    Packets: Sent = 2, Received = 2, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```
**Wireshark it**

*Type de packet envoyé 8 (echo request) Ping*

*Type de packet envoyé 0 (echo reply) Pong*

[PCAP du ping](./PCAP/ping.pcapng)

## II. ARP my bro
Check the ARP table
```bash=
PS C:\WINDOWS\system32> arp -a
Interface: 169.254.64.194 --- 0x5
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  ...
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 10.188.92.67 --- 0xa
  Internet Address      Physical Address      Type
  10.188.0.1            50-9a-4c-65-61-0d     dynamic
  10.188.4.240          7c-2a-31-8b-33-87     dynamic
  ...
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static
  
Interface: 192.168.0.1 --- 0x10
  Internet Address      Physical Address      Type
  192.168.0.2           d8-bb-c1-20-21-c0     dynamic
  192.168.0.63          ff-ff-ff-ff-ff-ff     static
  ...
```
*Adresse MAC du binome : d8-bb-c1-20-21-c0 (avec l'ip 192.168.0.2)*

MAC de la gateway de mon réseau WiFi
```bash=
PS C:\WINDOWS\system32> ipconfig

Windows IP Configuration
...
Wireless LAN adapter Wi-Fi:

   Connection-specific DNS Suffix  . :
   IPv4 Address. . . . . . . . . . . : 10.33.19.88
   Subnet Mask . . . . . . . . . . . : 255.255.252.0
   Default Gateway . . . . . . . . . : 10.33.19.254
PS C:\WINDOWS\system32> arp -a
...
Interface: 10.33.19.88 --- 0xa
  Internet Address      Physical Address      Type
  ...
  10.33.19.254          00-c0-e7-e0-04-4e     dynamic
```
*On trouve dans un premier temps l'adresse IP de la Gateway pour la retrouver dans la table ARP*

Manipuler la table ARP

```bash=
PS C:\WINDOWS\system32> netsh interface IP delete arpcache
Ok.

PS C:\WINDOWS\system32> arp -a
...
Interface: 192.168.0.1 --- 0x10
  Internet Address      Physical Address      Type

PS C:\WINDOWS\system32> ping 192.168.0.2

Pinging 192.168.0.2 with 32 bytes of data:
Reply from 192.168.0.2: bytes=32 time=2ms TTL=64
Reply from 192.168.0.2: bytes=32 time=1ms TTL=64
Reply from 192.168.0.2: bytes=32 time=1ms TTL=64

Ping statistics for 192.168.0.2:
    Packets: Sent = 3, Received = 3, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 1ms, Maximum = 2ms, Average = 1ms
Control-C
PS C:\WINDOWS\system32> arp -a
...
Interface: 192.168.0.1 --- 0x10
  Internet Address      Physical Address      Type
  192.168.0.2           d8-bb-c1-20-21-c0     dynamic
  192.168.0.63          ff-ff-ff-ff-ff-ff     static
```
*On vide la table ARP puis on constate qu'elle est vide pour ensuite ping la machine 2 pour reobtenir des informations dans la table ARP (que l'on reaffiche)*

**Wireshark it**

[PCAP pour les trames ARP](./PCAP/tramesARP.pcapng)

1er trame (ARP broadcast): 
- Adresse source (70:8b:cd :10:2a:d6) Mon ordinateur
- Destination:  (ff:ff:ff:ff:ff:ff) le Broadcast pour contacter tous les appareils sur le réseau

2eme trame (ARP reply):
- Source: (70:8b:cd :10:2a:d6) Mon ordinateur
- Destination:(d8:bb:c1:20:21:c0) l'ordinateur du binome

## III. DHCP you too my brooo

**Wireshark it**

[PCAP pour l'échange DORA](./PCAP/DORA.pcapng)

Trame 1 (Discover):
- Source : 169.254.86.76 Adresse de l'ordinateur auto attribuée APIPA (n'ayant pas de serveur DHCP mais une simple box)
- Destination : 255.255.255.255 Broadcast

Trame 2 (Offer):
- Source : 10.188.0.1 Adresse du serveur DHCP
- Destination : Broadcast

Trame 3 (Request):
- Source : 169.254.86.76 Adresse de l'ordinateur
- Destination : Broadcast

Trame 4 (Ack):
- Source : 10.188.0.1 Adresse du serveur DHCP
- Destination : Broadcast

Les différentes informations qui ont pu être transmises sont :
- Ip à utiliser : 10.188.92.67 (connaissable à partir de Offer)
- Adresse IP de la Gateway : 10.188.0.1
- Adresse IP d'un serveur DNS : 10.188.0.1 (La même que la gateway et du serveur DHCP car c'est une box qui joue leurs rôles)

*La première et troisième destination sont le broadcast car l'ordinateur cherche à contacter un serveur DHCP mais ne sachant pas ou il est (si il y en a un au début) il envoie donc les requêtes partout.
Les autres destinations sont également en direction du Broadcast mais provenant du serveur DHCP cherchant le client.*

## IV. Avant-goût TCP et UDP
**Wireshark it**

[PCAP pour identifier les infos](./PCAP/yt.pcapng)

*Je pense que le port utilisé est le 443, avec l'ip 173.194.30.39*
