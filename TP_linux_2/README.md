# TP2 : Gestion de service
## I. Un premier serveur web

### 1. Installation
**Installer le serveur Apache**
```bash=
[hugo@web ~]$ sudo dnf -y install httpd
...
Complete!
[hugo@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
```
**Démarrer le service Apache**
```bash=
[hugo@web ~]$ sudo systemctl start httpd
[hugo@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[hugo@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=1213,fd=4),("httpd",pid=1212,fd=4),("httpd",pid=1211,fd=4),("httpd",pid=1209,fd=4))
[hugo@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --reload
success
```
**TEST**
```bash=
[hugo@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 10:09:50 CET; 5min ago
[hugo@web ~]$ systemctl is-enabled httpd
enabled
[hugo@web ~]$ curl localhost:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
...
```
Sur ma machine (Windows):
```bash=
PS C:\WINDOWS\system32> curl 10.102.1.11:80
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
...
```
### 2. Avancer vers la maîtrise du service
**Le service Apache...**
```bash=
[hugo@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```
**Déterminer sous quel utilisateur tourne le processus Apache**
```bash=
[hugo@web ~]$ cat /etc/httpd/conf/httpd.conf | grep -iF user
User apache
[hugo@web ~]$ ps -ef | grep httpd
root        1209       1  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1210    1209  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1211    1209  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1212    1209  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1213    1209  0 10:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[hugo@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Nov 15 10:01 .
drwxr-xr-x. 81 root root 4096 Nov 15 10:01 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```
Le service tourne donc sous l'utilisateur apache et on remarque que les droits des 'others' et bien au moins en lecture sur le contenu (par ex:index.html) et sur le répertoire.
**Changer l'utilisateur utilisé par Apache**
```bash=
[hugo@web ~]$ cat /etc/passwd | grep apache
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
[hugo@web ~]$ sudo useradd userapache -m -d /usr/share/httpd -s /sbin/nologin
[hugo@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[hugo@web ~]$ cat /etc/httpd/conf/httpd.conf | grep -iF user
User userapache
[hugo@web ~]$ sudo systemctl restart httpd
[hugo@web ~]$ ps -ef | grep httpd
root        1554       1  0 10:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
userapa+    1555    1554  0 10:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
userapa+    1556    1554  0 10:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
userapa+    1557    1554  0 10:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
userapa+    1558    1554  0 10:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
Comme j'ai appelé mon utilisateur : userapache et que c'est trop long ... il y a marqué userapa+ mais c'est bien le nouvel utilisateur qui fait tourner le service.
**Faites en sorte que Apache tourne sur un autre port**
```bash=
[hugo@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[hugo@web ~]$ cat /etc/httpd/conf/httpd.conf | grep -iF listen
Listen 6009
[hugo@web ~]$ sudo firewall-cmd --add-port=6009/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --reload
success
[hugo@web ~]$ sudo systemctl restart httpd
[hugo@web ~]$ ss -alnpt
State       Recv-Q      Send-Q            Local Address:Port             Peer Address:Port      Process
LISTEN      0           128                     0.0.0.0:22                    0.0.0.0:*
LISTEN      0           128                        [::]:22                       [::]:*
LISTEN      0           511                           *:6009                        *:*
[hugo@web ~]$ curl localhost:6009
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
...
```
Sur windows:
```bash=
PS C:\WINDOWS\system32> curl 10.102.1.11:6009
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
...
```
📁 [httpd.conf](./file/httpd.conf)

## II. Une stack web plus avancée
### 2. Setup
#### A. Base de données
**Install de MariaDB sur db.tp2.linux**
```bash=
[hugo@db ~]$ sudo dnf install mariadb-server
...
Complete!
[hugo@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[hugo@db ~]$ sudo systemctl start mariadb
[hugo@db ~]$ sudo mysql_secure_installation
...

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] y
Enabled successfully!
Reloading privilege tables..
 ... Success!


You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
[hugo@db ~]$ ss -alnpt
State       Recv-Q      Send-Q            Local Address:Port             Peer Address:Port      Process
LISTEN      0           128                     0.0.0.0:22                    0.0.0.0:*
LISTEN      0           80                            *:3306                        *:*
LISTEN      0           128                        [::]:22                       [::]:*
[hugo@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[hugo@db ~]$ sudo firewall-cmd --reload
success
```
**Préparation de la base pour NextCloud**
```bash=
[hugo@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.005 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.005 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```
**Exploration de la base de données**
```bash=
[hugo@web ~]$ sudo dnf install mysql -y
Complete!
[hugo@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
...
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.01 sec)

mysql> use nextcloud;
Database changed
mysql> show tables;
Empty set (0.01 sec)
```
**Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**
```bash=
[hugo@db ~]$ sudo mysql -u root -p
...
mysql> select user from mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
4 rows in set (0.00 sec)
```
#### B. Serveur Web et NextCloud
**Install de PHP**
```bash=
[hugo@web ~]$ sudo dnf config-manager --set-enabled crb
[hugo@web ~]$ sudo dnf install dnf-utils
...
Complete!
[hugo@web ~]$ dnf module list php
...
Name             Stream               Profiles                              Summary
php              remi-7.4             common [d], devel, minimal            PHP scripting language
php              remi-8.0             common [d], devel, minimal            PHP scripting language
php              remi-8.1             common [d], devel, minimal            PHP scripting language
php              remi-8.2             common [d], devel, minimal            PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
[hugo@web ~]$ sudo dnf module enable php:remi-8.1 -y
...
Complete!
[hugo@web ~]$ sudo dnf install -y php81-php
...
Complete!
```
**Install de tous les modules PHP nécessaires pour NextCloud**
```bash=
sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
...
Complete!
```
**Récupérer NextCloud**
```bash=
[hugo@web ~]$ sudo mkdir /var/www/tp2_nextcloud/
[hugo@web ~]$ curl -o nextcloudfile.zip https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  168M  100  168M    0     0  3294k      0  0:00:52  0:00:52 --:--:-- 3415k
[hugo@web ~]$ ls
nextcloudfile.zip
[hugo@web ~]$ sudo dnf install unzip -y
...
Complete!
[hugo@web ~]$ sudo mv nextcloudfile.zip /var/www/tp2_nextcloud/
[hugo@web ~]$ sudo unzip /var/www/tp2_nextcloud/nextcloudfile.zip -d /var/www/tp2_nextcloud/
...
[hugo@web ~]$ sudo mv /var/www/tp2_nextcloud/nextcloud/* /var/www/tp2_nextcloud/
[hugo@web ~]$ sudo rm -r /var/www/tp2_nextcloud/nextcloud
[hugo@web ~]$ ls /var/www/tp2_nextcloud/
3rdparty  config       core      index.html  occ           ocs-provider  resources   themes
apps      console.php  cron.php  index.php   ocm-provider  public.php    robots.txt  updater
AUTHORS   COPYING      dist      lib         ocs           remote.php    status.php  version.php
[hugo@web ~]$ sudo chown -R apache /var/www/tp2_nextcloud/
[hugo@web ~]$ ls -al /var/www/tp2_nextcloud/
total 140
drwxr-xr-x. 14 apache root  4096 Oct  6 14:47 .
drwxr-xr-x.  3 root   root    48 Nov 15 12:43 ..
...
drwxr-xr-x.  2 apache root  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache root  3253 Oct  6 14:42 .htaccess
-rw-r--r--.  1 apache root   156 Oct  6 14:42 index.html
...
```
**Adapter la configuration d'Apache**
```bash=
[hugo@web ~]$ sudo vim /etc/httpd/conf.d/nextcloud.conf
[hugo@web ~]$ cat /etc/httpd/conf.d/nextcloud.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
**Redémarrer le service Apache pour qu'il prenne en compte le nouveau fichier de conf**
```bash=
[hugo@web ~]$ sudo systemctl restart httpd
```
#### C. Finaliser l'installation de NextCloud
**Exploration de la base de données**
```bash=
[hugo@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
...
mysql> SELECT count(*) from information_schema.tables WHERE table_schema = 'nextcloud';
+----------+
| count(*) |
+----------+
|       95 |
+----------+
1 row in set (0.00 sec)
```