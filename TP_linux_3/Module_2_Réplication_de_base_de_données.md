# Module 2 : Réplication de base de données
### Setup serveur Replication
```bash=
[hugo@replication ~]$ sudo dnf install mariadb-server -y
...
Complete!
[hugo@replication ~]$ sudo systemctl start mariadb
[hugo@replication ~]$ sudo systemctl enable mariadb
[hugo@replication ~]$ sudo mysql_secure_installation
...
Thanks for using MariaDB!
```
On installe mariadb sur le serveur de réplication.
### Conf Master
```bash=
[hugo@db ~]$ sudo vim /etc/my.cnf
[hugo@db ~]$ sudo cat /etc/my.cnf
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
[mariadb]
log-bin
server-id=1
log-basename=master1
binlog-format=mixed
skip-networking=0
bind-address=0.0.0.0
[hugo@db ~]$ sudo systemctl restart mariadb
[hugo@db ~]$ sudo mysql -u root -p
...
MariaDB [(none)]> create user 'replication'@'10.102.1.15' identified by '1234';
Query OK, 0 rows affected (0.007 sec)

MariaDB [(none)]> grant replication slave on *.* to 'replication'@'10.102.1.15';
Query OK, 0 rows affected (0.006 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> show master status;
+--------------------+----------+--------------+------------------+
| File               | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+--------------------+----------+--------------+------------------+
| master1-bin.000002 |     1094 |              |                  |
+--------------------+----------+--------------+------------------+
1 row in set (0.000 sec)
```
#### Bonus : Faire en sorte que l'utilisateur créé en base de données ne soit utilisable que depuis l'autre serveur de base de données
On créer l'utilisateur replication accessible seulement via la machine de réplication.   
On récupère au passage quelques informations utiles pour la suite avec `show master status`.
### Conf Slave
```bash=
[hugo@replication ~]$ sudo vim /etc/my.cnf
[sudo] password for hugo:
[hugo@replication ~]$ cat /etc/my.cnf
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d

[mariadb]
bind-address=0.0.0.0
server-id=2
log-bin
log-basename=slave1
binlog-format=mixed
skip-networking=0
[hugo@replication ~]$ sudo systemctl restart mariadb
[hugo@replication ~]$ sudo mysql -u root -p
...
MariaDB [(none)]> stop slave;
Query OK, 0 rows affected, 1 warning (0.000 sec)

MariaDB [(none)]> change master to master_host = '10.102.1.12', master_user = 'replication', master_pass
word = '1234', master_log_file = 'master1-bin.000002', master_log_pos = 1094;
Query OK, 0 rows affected (0.026 sec)

MariaDB [(none)]> start slave;
Query OK, 0 rows affected (0.002 sec)
```
Après l'avoir configuré comme le master serveur, on indique à mariadb le serveur à repliquer ainsi que les identifiants de connexion via l'user replication ici.  
On démarre le "slave".
### Test
```bash=
[hugo@db ~]$ sudo mysql -u root -p
...
MariaDB [(none)]> create database test;
Query OK, 1 row affected (0.000 sec)
```
On fais une action sur le serveur database pour regarder si il se passe la même chose sur le serveur de réplication.
```bash=
[hugo@replication ~]$ sudo mysql -u root -p
...
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| test               |
+--------------------+
4 rows in set (0.001 sec)
```
L'action est également réalisé sur le serveur de réplication donc la réplication marche.