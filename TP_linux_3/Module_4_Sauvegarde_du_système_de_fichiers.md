# Module 4 : Sauvegarde du système de fichiers
## I. Script de backup
### 1. Ecriture du script
```bash=
[hugo@web ~]$ sudo dnf install zip -y
Complete!
[hugo@web ~]$ cat /srv/tp3_backup.sh
#!/bin/bash
#21/11/22 by hugo
#This script makes backup of nextcloud files

datestr=(`date +%y%m%d%H%M%S`)
name_file="nextcloud_"$datestr""
mkdir -p /srv/backup/
zip -q -r /srv/backup/"$name_file".zip /var/www/tp2_nextcloud/config/ /var/www/tp2_nextcloud/data/ /var/www/tp2_nextcloud/themes
```
#### Environnement d'exécution du script
```bash=
[hugo@web ~]$ sudo useradd backup -m -d /srv/backup/ -s /usr/bin/nologin
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
useradd: warning: the home directory /srv/backup/ already exists.
useradd: Not copying any file from skel directory into it.
[hugo@web ~]$ sudo chown backup /srv/backup/
[hugo@web ~]$ sudo -u backup sh /srv/tp3_backup.sh
[hugo@web ~]$ ls /srv/backup/
nextcloud_221121130538.zip
```
Une archive s'est bien créée dans le répertoire /srv/backup/
### 3. Service et timer
#### Créez un service système qui lance le script
```bash=
[hugo@web ~]$ sudo vim /etc/systemd/system/backup.service
[hugo@web ~]$ cat /etc/systemd/system/backup.service
[Unit]
Description=Execute tp3_backup for make backup of nextcloud files

[Service]
ExecStart=/srv/tp3_backup.sh
Type=oneshot
User=backup

[Install]
WantedBy=multi-user.target
[hugo@web ~]$ sudo systemctl start backup
[hugo@web ~]$ sudo systemctl status backup
...
Nov 21 16:26:44 web.tp2.linux systemd[1]: Starting Execute tp3_backup for make backup of nextcloud files...
Nov 21 16:26:44 web.tp2.linux systemd[1]: backup.service: Deactivated successfully.
Nov 21 16:26:44 web.tp2.linux systemd[1]: Finished Execute tp3_backup for make backup of nextcloud files.
```
#### Créez un timer système qui lance le service à intervalles réguliers
```bash=
[hugo@web ~]$ sudo vim /etc/systemd/system/backup.timer
[hugo@web ~]$ cat /etc/systemd/system/backup.timer
[Unit]
Description=Run service backup

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
[hugo@web ~]$ sudo systemctl daemon-reload
[hugo@web ~]$ sudo systemctl start backup.timer
[hugo@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[hugo@web ~]$ sudo systemctl status backup.timer
● backup.timer - Run service backup
     Loaded: loaded (/etc/systemd/system/backup.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Mon 2022-11-21 16:30:28 CET; 25s ago
      Until: Mon 2022-11-21 16:30:28 CET; 25s ago
    Trigger: Tue 2022-11-22 04:00:00 CET; 11h left
   Triggers: ● backup.service

Nov 21 16:30:28 web.tp2.linux systemd[1]: Started Run service backup.
[hugo@web ~]$ sudo systemctl list-timers
NEXT                        LEFT          LAST                        PASSED       UNIT                         ACTIVAT>
...
Tue 2022-11-22 04:00:00 CET 11h left      n/a                         n/a          backup.timer                 backup.>
...
```
Le timer fonctionne bien et lancera le service tous les jours à 4h
## II. NFS
### 1. Serveur 
#### Préparer un dossier à partager sur le réseau et sur la machine storage.tp3.linux
```bash=
[hugo@storage ~]$ sudo mkdir -p /srv/nfs_shares/web.tp2.linux/
```
#### Installer le serveur NFS
```bash=
[hugo@storage ~]$ sudo dnf install nfs-utils -y
...
Complete!
[hugo@storage ~]$ sudo vim /etc/exports
[hugo@storage ~]$ cat /etc/exports
/srv/nfs_shares/web.tp2.linux/ 10.102.1.11(rw,no_subtree_check,sync)
[sudo] password for hugo:
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[hugo@storage ~]$ sudo systemctl start nfs-server
[hugo@storage ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
[hugo@storage ~]$ sudo firewall-cmd --reload
success
```
Normalement l'ouverture du port 2049 suffit pour cette version de nfs
### 2. Client NFS
#### Installer un client NFS sur web.tp2.linux
```bash=
[hugo@web ~]$ sudo dnf install nfs-utils -y
...
Complete!
[hugo@web ~]$ sudo mount -t nfs 10.102.1.14:/srv/nfs_shares/web.tp2.linux/ /srv/backup/
[hugo@web ~]$ df -h
Filesystem                                 Size  Used Avail Use% Mounted on
...
10.102.1.14:/srv/nfs_shares/web.tp2.linux  6.2G  1.2G  5.1G  18% /srv/backup
[hugo@web ~]$ sudo vim /etc/fstab
[hugo@web ~]$ cat /etc/fstab
...
10.102.1.14:/srv/nfs_shares/web.tp2.linux/ /srv/backup nfs4 defaults 0 0
```
Le dossier est bien monté et est accesible.  
On teste si ça marche tout de même:
```bash=
[hugo@storage ~]$ sudo touch /srv/nfs_shares/web.tp2.linux/test
```
```bash=
[hugo@web ~]$ ls /srv/backup
test
```
Le fichier est bien aussi présent sur la machine web par exemple.
#### Tester la restauration des données sinon ça sert à rien :)
```bash=
[hugo@web ~]$ unzip /srv/backup/nextcloud_221122115521.zip
[hugo@web ~]$ mv var/www/tp2_nextcloud/ /var/www/tp2_nextcloud/
```