# Module 7 : Fail2Ban
```bash=
sudo dnf install epel-release -y
Complete!
sudo dnf install fail2ban -y
Complete!
```
On installe fail2ban.
#### Si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
```bash=
[hugo@db ~]$ cd /etc/fail2ban/
[hugo@db fail2ban]$ sudo cp jail.conf jail.local
[hugo@db fail2ban]$ sudo vim jail.local
[hugo@db fail2ban]$ cd
[hugo@db ~]$ sudo cat /etc/fail2ban/jail.local
...
# SSH servers
#

[sshd]
jail.local:
...
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
bantime = 1d
maxretry= 3
findtime= 1m
...
[hugo@db ~]$ sudo systemctl start fail2ban
[hugo@db ~]$ sudo systemctl enable fail2ban
```
On modifie et configure les différents paramètres dans la partie sshd pour qu'il convienne à ce que l'on souhaite. (Le nombre d'essai en combien de temps par exemple.)
#### Vérifiez que ça fonctionne en vous faisant ban
```bash=
[hugo@web ~]$ ssh hugo@10.102.1.12
...
hugo@10.102.1.12's password:
Permission denied, please try again.
hugo@10.102.1.12's password:
Permission denied, please try again.
hugo@10.102.1.12's password:
hugo@10.102.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[hugo@web ~]$ ssh hugo@10.102.1.12
ssh: connect to host 10.102.1.12 port 22: Connection refused
```
On remarque que le quatrième essai (après être banni) nous indique un status différents des essai précédents, plus particulièrement que la connexion a été refusée donc nous sommes bien banni de la machine.
#### Afficher la ligne dans le firewall qui met en place le ban
```bash=
[hugo@db ~]$ sudo iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
f2b-sshd   tcp  --  anywhere             anywhere             multiport dports ssh
...
Chain f2b-sshd (1 references)
target     prot opt source               destination
REJECT     all  --  10.102.1.11          anywhere             reject-with icmp-port-unreachable
RETURN     all  --  anywhere             anywhere
```
Je n'ai pas trouvé la ligne dans le firewall mais dans l'iptables. On peut voir que l'ip est bien en REJECT.
#### Lever le ban avec une commande liée à fail2ban
```bash=
[hugo@db ~]$ sudo fail2ban-client unban 10.102.1.11
1
```