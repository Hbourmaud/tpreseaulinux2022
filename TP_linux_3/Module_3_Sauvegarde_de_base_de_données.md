# Module 3 : Sauvegarde de base de données
## I. Script dump
#### Créer un utilisateur DANS LA BASE DE DONNEES
```bash=
[hugo@db ~]$ sudo mysql -u root -p
Enter password:
...
MariaDB [(none)]> CREATE USER 'dumpuser'@'localhost' IDENTIFIED BY '1234';
Query OK, 0 rows affected (0.007 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'dumpuser'@'localhost';
Query OK, 0 rows affected (0.005 sec)

MariaDB [(none)]> FLUSh privileges;
Query OK, 0 rows affected (0.002 sec)
```

### Ecrire le script bash & Clean it

```bash=
[hugo@db ~]$ sudo dnf install zip -y
...
Complete!
[hugo@db ~]$ sudo vim /srv/tp3_db_dump.sh
[hugo@db ~]$ sudo cat /srv/tp3_db_dump.sh
#!/bin/bash
# 18/11/22 by hugo
# This script dumps a database and compress in .zip to possibly restore it after in case of needed.
# Saves Folder at /srv/db_dumps/

db_user="dumpuser"
db_passwd="1234"
db_name="nextcloud"
db_ip="localhost"
datestr=(`date +%y%m%d%H%M%S`)
name_file="db_nextcloud_"$datestr""
mkdir -p /srv/db_dumps/
mysqldump -u dumpuser -h localhost -p1234 nextcloud > /srv/db_dumps/"$name_file".sql
cd /srv/db_dumps/
zip -q /srv/db_dumps/"$name_file".zip "$name_file".sql
rm /srv/db_dumps/"$name_file".sql
```
L'utilisation de variable et l'ajout des commentaires au début est fait ici.  
J'ai choisi d'archiver en .zip donc on installe le packet correspondant.
#### Environnement d'exécution du script
```bash=
[hugo@db ~]$ sudo useradd db_dumps -m -d /srv/db_dumps/ -s /usr/bin/nologin
[hugo@db ~]$ sudo -u db_dumps sh /srv/tp3_db_dump.sh
[hugo@db ~]$ ls -la /srv/db_dumps/
total 24
drwxr-xr-x. 2 db_dumps db_dumps    43 Nov 18 11:54 .
drwxrwxrwx. 3 root     root        44 Nov 18 11:53 ..
-rw-r--r--. 1 db_dumps db_dumps 24554 Nov 18 11:54 db_nextcloud_221118115431.zip
[hugo@db ~]$ sudo chown db_dumps /srv/tp3_db_dump.sh
```
On gère les permissions de l'user sur le fichier.
#### Bonus : Ajoutez une gestion d'options au script
```bash=
[hugo@db ~]$ sudo vim /srv/tp3_db_dump.sh
[hugo@db ~]$ sudo -u db_dumps sh /srv/tp3_db_dump.sh -D nextcloud
[hugo@db ~]$ sudo cat /srv/tp3_db_dump.sh
...
while getopts ":U:P:D:I:h" option
do
        case $option in
                h)
                        echo -e "-U User for the database\n-P Password of the user\n-D Name of the database\n-I IP of the database"
                              ;;
                U)
                        db_user=$OPTARG
                        ;;
                P)
                        db_passwd=$OPTARG
                        ;;
                D)
                        db_name=$OPTARG
                        ;;
                I)
                        db_ip=$OPTARG
                        ;;
        esac
done
...
```
#### Bonus : Stocker le mot de passe pour se co à la base dans un fichier séparé
```bash=
[hugo@db ~]$ sudo vim /srv/db_pass
[hugo@db ~]$ sudo cat /srv/db_pass
db_passwd="1234"
[hugo@db ~]$ sudo vim /srv/tp3_db_dump.sh
...
db_user="dumpuser"
source /srv/db_pass
db_name="nextcloud"
...
```
### III. Service et timer
#### Créez un service système qui lance le script
```bash=
[hugo@db ~]$ sudo vim /etc/systemd/system/db-dump.service
[hugo@db ~]$ sudo cat /etc/systemd/system/db-dump.service
[Unit]
Description=Execute tp3_db_dump for make backup of nextcloud database

[Service]
ExecStart=/srv/tp3_db_dump.sh
Type=oneshot
User=db_dumps

[Install]
WantedBy=multi-user.target

[hugo@db ~]$ sudo systemctl start db-dump
[hugo@db ~]$ sudo systemctl status db-dump
...
Nov 19 16:49:19 db.tp2.linux systemd[1]: Starting Execute tp3_db_dump for make backup of nextcloud database...
Nov 19 16:49:19 db.tp2.linux systemd[1]: db-dump.service: Deactivated successfully.
Nov 19 16:49:19 db.tp2.linux systemd[1]: Finished Execute tp3_db_dump for make backup of nextcloud database.
```
#### Créez un timer système qui lance le service à intervalles réguliers
```bash=
[hugo@db ~]$ sudo vim /etc/systemd/system/db-dump.timer
[hugo@db ~]$ cat /etc/systemd/system/db-dump.timer
[Unit]
Description=Run service db-dump

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
[hugo@db ~]$ sudo systemctl daemon-reload
[hugo@db ~]$ sudo systemctl start db-dump.timer
[hugo@db ~]$ sudo systemctl enable db-dump.timer
Created symlink /etc/systemd/system/timers.target.wants/db-dump.timer → /etc/systemd/system/db-dump.timer.
[hugo@db ~]$ sudo systemctl status db-dump.timer
● db-dump.timer - Run service db-dump
     Loaded: loaded (/etc/systemd/system/db-dump.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Sat 2022-11-19 17:07:03 CET; 30s ago
      Until: Sat 2022-11-19 17:07:03 CET; 30s ago
    Trigger: Sun 2022-11-20 04:00:00 CET; 10h left
   Triggers: ● db-dump.service

Nov 19 17:07:03 db.tp2.linux systemd[1]: Started Run service db-dump.
[hugo@db ~]$ sudo systemctl list-timers
NEXT                        LEFT          LAST                        PASSED       UNIT                         ACTIVAT>
...
Sun 2022-11-20 04:00:00 CET 9h left       n/a                         n/a          db-dump.timer                db-dump>
...
```
Un timer sera donc créer et permettra de lancer le script correspondant tous les jours à 4h.
#### Tester la restauration des données
```bash=
[hugo@db ~]$ mysql -u root -h localhost -p
...
MariaDB [(none)]> drop database nextcloud;
Query OK, 95 rows affected (0.697 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
3 rows in set (0.005 sec)
MariaDB [(none)]> CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)
exit
[hugo@db ~]$ cd /srv/db_dumps/
[hugo@db db_dumps]$ sudo unzip db_nextcloud_221119172620.zip
Archive:  db_nextcloud_221119172620.zip
  inflating: db_nextcloud_221119172620.sql
[hugo@db ~]$ sudo mysql -u dumpuser -p nextcloud < db_nextcloud_221119172620.sql
[hugo@db ~]$ sudo mysql -u dumpuser -p
...
MariaDB [(none)]> use nextcloud;
...
Database changed
MariaDB [nextcloud]> show tables;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
...
95 rows in set (0.001 sec)
```
Tout d'abord on drop la database déjà existante pour simuler sa perte.  
On créer une database du même nom mais "vide".  
Ensuite on indique qu'elle fichier utilisée pour restaurer la database, donc ici le fichier provient du unzip de l'archive de la dernière sauvegarde effectuée.  
On vérifie que la base de donnée contient bien les données et toutes ces tables.