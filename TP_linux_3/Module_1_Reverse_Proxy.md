# Module 1 : Reverse Proxy
### II. Setup NGINX
La machine proxy.tp3.linux a pour adresse IP 10.102.1.13
```
[hugo@proxy ~]$ sudo dnf install nginx
...
Complete!
[hugo@proxy ~]$ sudo systemctl start nginx
[hugo@proxy ~]$ sudo systemctl enable nginx
[hugo@proxy ~]$ ss -alnpt
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             511                        0.0.0.0:80                      0.0.0.0:*
...
[hugo@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[hugo@proxy ~]$ sudo firewall-cmd --reload
success
[hugo@proxy ~]$ ps -ef | grep nginx
root        1029       1  0 10:58 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1030    1029  0 10:58 ?        00:00:00 nginx: worker process
```
On installe NGINX puis on ajoute le port sur lequel il écoute au firewall.
```
[hugo@db ~]$ curl 10.102.1.13:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
...    
```
La page d'accueil est donc disponible sur le port 80.
#### Config NGINX
```
[hugo@proxy ~]$ cat /etc/nginx/nginx.conf
...
include /etc/nginx/conf.d/*.conf;
...
[hugo@proxy ~]$ sudo vim /etc/nginx/conf.d/proxyweb.conf
[hugo@proxy ~]$ cat /etc/nginx/conf.d/proxyweb.conf
...
        # On définit la cible du proxying
        proxy_pass http://10.102.1.11:80;
...
```
On créer le fichier de conf (ici nommé proxyweb) et on indique dans la configuration minimale donnée l'adresse correcte de nextcloud.
```
[hugo@web ~]$ sudo vim /var/www/tp2_nextcloud/config/config.php
[hugo@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
...
  'trusted_domains' =>
  array (
          0 => '10.102.1.13',
          1 => 'web.tp2.linux',
  ),
 ...
```
On ajoute dans la config de nextcloud dans les trusted domains l' adresse du proxy.
#### Modifier votre fichier hosts de VOTRE PC
Sur ma machine windows:
```
PS C:\Users\User> curl web.tp2.linux

Content           : <!DOCTYPE html>
                    <html>
                    <head>
...
```
#### Rendre le serveur web.tp2.linux injoignable sauf depuis l'IP du reverse proxy.
```
[hugo@web ~]$ sudo firewall-cmd --zone=public --remove-port=80/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --zone=block --change-interface=enp0s8 --permanent
success
[hugo@web ~]$ sudo firewall-cmd --permanent --new-zone=webaccess
success
[hugo@web ~]$ sudo firewall-cmd --zone=webaccess --add-source=10.102.1.13 --permanent
success
[hugo@web ~]$ sudo firewall-cmd --zone=webaccess --add-port=80/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --zone=block --add-port=22/tcp --permanent
success
[hugo@web ~]$ sudo firewall-cmd --reload
success
```
On enlève le port 80 de la zone publique pour qu'il n'y est plus rien de spécifier dans cette zone.  
On va changer la zone de l'interface utilisée en zone block pour bloquée toutes les connexions possibles mais on ajoute le port 22 pour éviter de perdre la connexion ssh  
On veut tout de fois que le proxy puisse communiquer avec la machine web donc on va créer une zone (appelée webaccess ici) en indiquant l'adresse IP du proxy et du port utilisée.
```
PS C:\Users\User> ping 10.102.1.13

Pinging 10.102.1.13 with 32 bytes of data:
Reply from 10.102.1.13: bytes=32 time<1ms TTL=64
Reply from 10.102.1.13: bytes=32 time<1ms TTL=64

Ping statistics for 10.102.1.13:
    Packets: Sent = 2, Received = 2, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
PS C:\Users\User> ping 10.102.1.11

Pinging 10.102.1.11 with 32 bytes of data:
Reply from 10.102.1.11: Destination net unreachable.
Reply from 10.102.1.11: Destination net unreachable.

Ping statistics for 10.102.1.11:
    Packets: Sent = 2, Received = 2, Lost = 0 (0% loss),
```
### III. HTTPS
On génére la paire de clé, que l'on renomme et que l'on déplace dans un endroit plus approprié.
```
[hugo@proxy ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
...
[hugo@proxy ~]$ sudo mv server.crt web.tp2.linux.crt
[hugo@proxy ~]$ sudo mv server.key web.tp2.linux.key
[hugo@proxy ~]$ sudo mv web.tp2.linux.crt /etc/pki/tls/certs/
[hugo@proxy ~]$ sudo mv web.tp2.linux.key /etc/pki/tls/private/
[hugo@proxy ~]$ sudo vim /etc/nginx/conf.d/proxyweb.conf
[hugo@proxy ~]$ sudo cat /etc/nginx/conf.d/proxyweb.conf
server {
        listen 80;
        server_name web.tp2.linux;
        return 301 https://$host$request_uri;
}

server {
...
    listen 443 ssl;
    ssl_certificate /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;
...

[hugo@proxy ~]$ sudo systemctl restart nginx

[hugo@proxy ~]$ sudo firewall-cmd --permanent --add-port=443/tcp
success
[hugo@proxy ~]$ sudo firewall-cmd --reload
success
```
On modifie la conf de nginx pour écouter sur le bon port et indiquer le chemin vers les clés en plus de rediriger les requêtes en http vers du https (en écoutant sur le port 80 pour rediriger directement vers du https).  
Ducoup on ajoute le port 443 au firewall.  
```
[hugo@web ~]$ sudo vim /var/www/tp2_nextcloud/config/config.php
[hugo@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
<?php
$CONFIG = array (
  'instanceid' => 'ocu71acnkqdy',
  'passwordsalt' => 'b3e4uvr+rfyTiCFyPKsiWUZg5Wc9Xa',
  'secret' => 'kxSu//qAc1k7jH6bLZz9w/e4Y3ilJO9h/a+4TS4RkCeijuMq',
  'trusted_domains' =>
  array (
          0 => '10.102.1.13',
          1 => 'web.tp2.linux',
  ),
  'overwritehost' => 'web.tp2.linux',
  'overwriteprotocol' => 'https',
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'https://web.tp2.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '10.102.1.12:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'oc_admin6',
  'dbpassword' => 'bb/+bNCf3F7^}1b4HmiJH@g+Z8y8_4',
  'installed' => true,
);
```
On modifie la conf de nextcloud pour faire marcher le tous correctement en obligeant à passer en https via web.tp2.linux (le hostname pas la machine).
