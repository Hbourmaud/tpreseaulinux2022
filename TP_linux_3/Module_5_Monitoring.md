# Module 5 : Monitoring

Les différentes commandes seront effectuées sur chaque machine que l'on souhaite monitorer. Donc web.tp2.linux et db.tp2.linux mais également certaines machines possiblement mis en place via les autres modules si nécessaire.

```bash=
[hugo@web ~]$ sudo dnf install epel-release -y
...
Complete!
[hugo@web ~]$ sudo dnf install wget -y
...
Complete!
wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
...
Complete!
...
[hugo@web ~]$ sudo systemctl start netdata
[hugo@web ~]$ sudo systemctl enable netdata
[hugo@web ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Sun 2022-11-20 14:21:24 CET; 2min 26s ago
...
[hugo@web ~]$ sudo ss -alnpt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1635,fd=51))

LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1635,fd=6))

LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=1635,fd=50))

LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=1635,fd=7))
[hugo@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent --zone=webaccess
success
[hugo@web ~]$ sudo firewall-cmd --reload
success
```
On installe les différents paquets utiles et netdata et on configure le parefeu pour le bon fonctionnement de netdata, ici on rajoute le port 19999 à la zone webaccess car la zone public ne sert plus dans la configuration mis en place ici.  
Maintenant on peut se rendre sur l'interface graphique de netdata via notre ordinateur sur le port 19999.
#### Configurer Netdata pour qu'il vous envoie des alertes dans un salon Discord
```bash=
[hugo@web ~]$ sudo /etc/netdata/edit-config health_alarm_notify.conf
[hugo@web ~]$ sudo systemctl restart netdata
```
On ajoute notre lien webhook discord dans la partie discord de la conf netdata
#### Vérifier que les alertes fonctionnent
```bash=
[hugo@web ~]$ sudo dnf install stress -y
...
Complete!
[hugo@web ~]$ sudo stress -c 8 -t 20s
[hugo@web ~]$ sudo stress --vm 2 --vm-bytes 500M
```
On installe *stress* qui permet d'effectuer différents stress test entre autres et on lance différentes surcharges pour tester le système d'alerte discord.  
Message reçu sur discord sur un channel:
```bash=
web.tp2.linux is critical, system.cpu (cpu), cpu = 100%
cpu = 100%
Anormal CPU utilization
system.cpu
cpu

web.tp2.linux•Aujourd’hui à 15:19
web.tp2.linux is critical, mem.available (system), ram available = 3.13%
ram available = 3.13%
percentage of estimated amount of RAM available for userspace processes, without causing swapping
mem.available
system

web.tp2.linux•Aujourd’hui à 15:19
web.tp2.linux needs attention, system.swapio (swap), 30min ram swapped out = 334.7% of RAM
30min ram swapped out = 334.7% of RAM
percentage of the system RAM swapped in the last 30 minutes
system.swapio
swap

web.tp2.linux•Aujourd’hui à 15:19
web.tp2.linux needs attention, system.ram (ram), ram in use = 90.7%
ram in use = 90.7%
system memory utilization
system.ram
ram

web.tp2.linux•Aujourd’hui à 15:19
```