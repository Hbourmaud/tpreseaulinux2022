# TP4 : Conteneurs
## I. Docker
`docker1.tp4.linux : 10.104.1.11/24`
### 1. Install
**Installer Docker sur la machine**
```bash=
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[hugo@docker1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
...
Complete!
[hugo@docker1 ~]$ sudo systemctl start docker
[hugo@docker1 ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[hugo@docker1 ~]$ sudo usermod -aG docker $(whoami)
```
### 3. Lancement de conteneurs
**Utiliser la commande docker run**
```bash=
[hugo@docker1 ~]$ sudo mkdir /etc/nginx/
[hugo@docker1 ~]$ sudo mkdir /etc/nginx/conf/
[hugo@docker1 ~]$ sudo mkdir /etc/nginx/html/
[hugo@docker1 ~]$ sudo vim /etc/nginx/conf/confperso.conf
[hugo@docker1 ~]$ sudo cat /etc/nginx/conf/confperso.conf
server {
        listen 80;
        root /usr/share/nginx/html;
}
[hugo@docker1 ~]$ sudo vim /etc/nginx/html/index.html
[hugo@docker1 ~]$ sudo cat /etc/nginx/html/index.html
<h1>Juste un test</h1>
[hugo@docker1 /]$ docker run --name web -d -v /etc/nginx/conf/:/etc/nginx/conf.d/ -v /etc/nginx/html/:/usr/share/nginx/html -p 8888:80 --memory 512mb --cpus 1 nginx
[hugo@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS
 NAMES
a9b792b3f01e   nginx     "/docker-entrypoint.…"   3 minutes ago   Up 3 minutes   0.0.0.0:8888->80/tcp, :::8888->80/tcp   web
[hugo@docker1 ~]$ curl 172.17.0.2:80
<h1>Juste un test</h1>
```
On créer les répertoires et fichiers correspondant que l'on souhaite utiliser dans le conteneur.  
On utilise certaines options pour utiliser nos fichiers créers dans le docker et on test si cela marche.  
On peut également se rendre sur la page depuis notre ordinateur et voir le résultat.
## II. Images
### 2. Construisez votre propre Dockerfile
**Construire votre propre image**
```bash=
[hugo@docker1 ~]$ mkdir workspace && cd workspace
[hugo@docker1 workspace]$ vim Dockerfile
[hugo@docker1 workspace]$ cat Dockerfile
FROM ubuntu

RUN apt update -y

RUN apt install apache2 -y

COPY var/www/html/ var/www/html

COPY etc/apache2/ etc/apache2

EXPOSE 80

CMD ["apache2ctl","-D","FOREGROUND"]
[hugo@docker1 workspace]$ mkdir -p var/www/html/
[hugo@docker1 workspace]$ mkdir -p etc/apache2/
[hugo@docker1 workspace]$ mkdir etc/apache2/logs/
[hugo@docker1 workspace]$ vim etc/apache2/apache2.conf
[hugo@docker1 workspace]$ cat etc/apache2/apache2.conf
# on définit un port sur lequel écouter
Listen 80
...
# quelques paramètres pour les logs
ErrorLog "logs/error.## log"
LogLevel warn
[hugo@docker1 workspace]$ vim var/www/html/index.html
[hugo@docker1 workspace]$ cat var/www/html/index.html
<h1>Deuxieme petit test</h1>
[hugo@docker1 workspace]$ docker build . -t my_own_apache
...
Successfully built 7268ba7a40f9
Successfully tagged my_own_apache:latest
[hugo@docker1 workspace]$ docker run -p 8888:80 my_own_apache
[hugo@docker1 ~]$ curl localhost:8888
<h1>Deuxieme petit test</h1>
```
Ici on utilisera une image ubuntu de base pour créer une image "my_own_apache".  
On run et on test si tous marche correctement, comme pour au dessus on peut y accèder également via notre ordinateur.
📁 [Dockerfile](./Dockerfile)
## III. docker-compose
### 2. Make your own meow
**Conteneurisez votre application**
```bash=
[hugo@docker1 ~]$ sudo dnf install git -y
[hugo@docker1 ~]$ mkdir workspace_myapp/file_app -p && cd workspace_myapp/file_app
[hugo@docker1 file_app]$ git clone https://github.com/Hbourmaud/Hangman-web
[hugo@docker1 workspace_myapp]$ vim Dockerfile
[hugo@docker1 workspace_myapp]$ cat Dockerfile
FROM golang

WORKDIR /app

EXPOSE 8080

COPY ./file_app/Hangman-web ./

CMD go run serv/main.go
[hugo@docker1 workspace_myapp]$ docker build . -t hangman-web
...
Successfully built 0b5fcf326982
Successfully tagged hangman-web:latest
[hugo@docker1 workspace_myapp]$ docker run -d --network=host hangman-web
[hugo@docker1 workspace_myapp]$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED          STATUS          PORTS     NAMES
0190f1968306   hangman-web   "/bin/sh -c 'go run …"   59 seconds ago   Up 58 seconds             crazy_rosalind
[hugo@docker1 workspace_myapp]$ curl localhost:8080
...
        <title>Hangman-web</title>
        <link rel="stylesheet" href="../css/style.css" />
    </head>
    <body class="startAll">
        <form method="POST" action="/hangman">
            <h1 class="police"> Choose a name :</h1><br>
              <label class="choix">
                <input class="espace" type="text" name="Username" required autocomplete="off">
              </label>
...
```
On installe git puis on créer un nouveau workspace.  
Pour l'application j'ai choisi le projet hangman-web réalisé l'année dernière en GO.  
(Dispo ici:)
```bash=
git clone https://github.com/Hbourmaud/Hangman-web
```
On créer notre Dockerfile correspondant et on le build.  
Dans la commande docker run on rajoute l'option --network=host pour pouvoir accèder à la page depuis la vm directement sans aller dans le docker.  
Le docker tourne bien et on peut accèder correctement à la page via un curl.
```bash=
[hugo@docker1 workspace_myapp]$ vim docker-compose.yml
[hugo@docker1 workspace_myapp]$ cat docker-compose.yml
version: "1"

services:
  hangmanweb:
    image: hangman-web
    build: Dockerfile
    network_mode: "host"
[hugo@docker1 workspace_myapp]$ docker compose up -d
[+] Running 1/1
 ⠿ Container workspace_myapp-hangmanweb-1  Started                                                                 0.2s
[hugo@docker1 workspace_myapp]$ curl localhost:8080
<!DOCTYPE html>
<html>
    <head>
        <title>Hangman-web</title>
        <link rel="stylesheet" href="../css/style.css" />
    </head>
...
```
On créer notre fichier docker-compose en précisant l'image que l'on souhaite utiliser et le Dockerfile tout en rajoutant l'option du network host.  
On regarde si le docker est bien lancée puis on accède à la page.
📁📁 [Dockerfile](./app/Dockerfile) [Docker-Compose](./app/docker-compose.yml)